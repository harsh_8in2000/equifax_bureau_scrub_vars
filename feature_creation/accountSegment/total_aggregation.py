#
import pandas as pd
import numpy as np
from functools import reduce
from utils import util_func as func
from configs.constant import delinquencies,phd_var_list,var_list,writtenof_status, dpd_var_list, date_list_for_emi
from utils.time_calculator import timeit
from datetime import datetime, date
import sys
import traceback


class TOTAL_AGGREGATE():
    @timeit
    def __init__(self,dc):
        # try:
        self.total_loan_amt_cnt = self.total_calc_loan_amt_count(dc._acctSegCl)
        self.calc_growth_loan_amt_count()
        # self.total_app_data_wof_count = self.total_calc_writtenoff_cnt(dc._acctSegCl)

        # self.total_app_max_dpd = self.total_calc_max_dpd(dc._acctSegCl)
        # self.total_app_delays = self.total_calc_delay_emi_cnts(dc._acctSegCl)
        # self.outstanding_agg = self.total_current_outstanding_balance(dc._acctSegCl)

        self.total_youngest_loan_df = self.total_youngest_loan(dc._acctSegCl)
        self.total_oldest_loan_df = self.total_oldest_loan(dc._acctSegCl)
        # self.total_delay_amount_df = self.total_delay_amount(dc._acctSegCl)
        # self.total_last_deliquence_df = self.new_total_last_delinq(dc._acctSegCl)

        self.credit_card_usage = self.credit_card_analysis(dc._acctSegCl)
        self.overdraft_usage = self.overdraft_analysis(dc._acctSegCl)
        self.account_dpd_ratio = self.total_dpd_ratio(dc._acctSegCl)
        self.impute_income_df = self.impute_total_income(dc._acctSegCl)

        self.score = dc._acctSegCl.groupby('appid').agg({'bureau_score': np.min, 'restructuring_flag': np.sum, 'writtenoff_flag': np.sum}).reset_index()
        self.score.rename(columns={'restructuring_flag': 'restructure_count', 'writtenoff_flag': 'written_off_count'}, inplace=True)

        tot_dfs = [self.total_loan_amt_cnt, self.total_youngest_loan_df, self.total_oldest_loan_df, self.credit_card_usage, self.overdraft_usage, self.account_dpd_ratio, self.impute_income_df, self.score]
        self.total_cibil_loans = reduce(lambda left, right: pd.merge(left, right, on=['appid'], how='outer'), tot_dfs)
        self.bureau_thick_bad_calc()
        # except Exception as ex:
        #     exc_type, exc_obj, exc_tb = sys.exc_info()
        #     error = traceback.extract_tb(exc_tb)
        #     print(error)
        #     print(ex)
        pass

    def total_calc_loan_amt_count(self, cibil_df):
        app_data_amt_count_ever = cibil_df.groupby(['appid']).agg({'sanctionamount': [np.sum, func.count_nonzero_nonnan, np.min, np.max]}).reset_index()
        app_data_amt_count_ever.columns = ['appid', 'disb_loan_amt_ever','disb_loan_cnt_ever','min_disb_loan_amt_ever', 'max_disb_loan_amt_ever']

        app_data_amt_count_3m_since_disbursed = cibil_df[cibil_df['age_of_loan'] <= 90].groupby(['appid']).agg({'sanctionamount': [np.sum, func.count_nonzero_nonnan, np.min, np.max]}).reset_index()
        app_data_amt_count_3m_since_disbursed.columns = ['appid', 'disb_amt_3m', 'disb_cnt_3m','min_disb_amt_3m','max_disb_amt_3m']

        app_data_amt_count_6m_since_disbursed = cibil_df[cibil_df['age_of_loan'] <= 180].groupby(['appid']).agg({'sanctionamount': [np.sum, func.count_nonzero_nonnan, np.min, np.max]}).reset_index()
        app_data_amt_count_6m_since_disbursed.columns = ['appid', 'disb_amt_6m', 'disb_cnt_6m','min_disb_amt_6m','max_disb_amt_6m']

        app_data_amt_count_9m_since_disbursed = cibil_df[cibil_df['age_of_loan'] <= 270].groupby(['appid']).agg({'sanctionamount': [np.sum, func.count_nonzero_nonnan, np.min, np.max]}).reset_index()
        app_data_amt_count_9m_since_disbursed.columns = ['appid', 'disb_amt_9m', 'disb_cnt_9m','min_disb_amt_9m','max_disb_amt_9m']

        app_data_amt_count_12m_since_disbursed = cibil_df[cibil_df['age_of_loan'] <= 365].groupby(['appid']).agg({'sanctionamount': [np.sum, func.count_nonzero_nonnan, np.min, np.max]}).reset_index()
        app_data_amt_count_12m_since_disbursed.columns = ['appid', 'disb_amt_12m', 'disb_cnt_12m','min_disb_amt_12m','max_disb_amt_12m']

        app_data_amt_count_18m_since_disbursed = cibil_df[cibil_df['age_of_loan'] <= 540].groupby(['appid']).agg({'sanctionamount': [np.sum, func.count_nonzero_nonnan, np.min, np.max]}).reset_index()
        app_data_amt_count_18m_since_disbursed.columns = ['appid', 'disb_amt_18m', 'disb_cnt_18m','min_disb_amt_18m','max_disb_amt_18m']

        app_data_amt_count_24m_since_disbursed = cibil_df[cibil_df['age_of_loan'] <= 730].groupby(['appid']).agg({'sanctionamount': [np.sum, func.count_nonzero_nonnan, np.min, np.max]}).reset_index()
        app_data_amt_count_24m_since_disbursed.columns = ['appid', 'disb_amt_24m', 'disb_cnt_24m','min_disb_amt_24m','max_disb_amt_24m']

        # app_data_amt_count_36m_since_disbursed = cibil_df[cibil_df['age_of_loan'] <= 1080].groupby(['appid']).agg({'sanctionamount': [np.sum, func.count_nonzero_nonnan, np.min, np.max]}).reset_index()
        # app_data_amt_count_36m_since_disbursed.columns = ['appid', 'disb_amt_36m', 'disb_cnt_36m','min_disb_amt_36m','max_disb_amt_36m']
        #
        # app_data_amt_count_48m_since_disbursed = cibil_df[cibil_df['age_of_loan'] <= 1440].groupby(['appid']).agg({'sanctionamount': [np.sum, func.count_nonzero_nonnan, np.min, np.max]}).reset_index()
        # app_data_amt_count_48m_since_disbursed.columns = ['appid', 'disb_amt_48m', 'disb_cnt_48m','min_disb_amt_48m','max_disb_amt_48m']

        dfs = [app_data_amt_count_ever,app_data_amt_count_3m_since_disbursed,app_data_amt_count_6m_since_disbursed,app_data_amt_count_9m_since_disbursed,app_data_amt_count_12m_since_disbursed,app_data_amt_count_18m_since_disbursed,app_data_amt_count_24m_since_disbursed] #app_data_amt_count_ever,app_data_amt_count_3m,app_data_amt_count_6m,app_data_amt_count_12m,
        app_data_amt_count = reduce(lambda left, right: pd.merge(left, right, on=['appid'], how='outer'), dfs)

        return app_data_amt_count
    
    def calc_growth_loan_amt_count(self):
        self.total_loan_amt_cnt['loan_amt_growth_18m_12m'] = (self.total_loan_amt_cnt['disb_amt_18m'] - self.total_loan_amt_cnt['disb_amt_12m']) / self.total_loan_amt_cnt['disb_amt_18m']
        self.total_loan_amt_cnt['loan_amt_growth_12m_6m'] = (self.total_loan_amt_cnt['disb_amt_12m'] - self.total_loan_amt_cnt['disb_amt_6m']) / self.total_loan_amt_cnt['disb_amt_12m']
        self.total_loan_amt_cnt['loan_amt_growth_6m_3m'] = (self.total_loan_amt_cnt['disb_amt_6m'] - self.total_loan_amt_cnt['disb_amt_3m']) / self.total_loan_amt_cnt['disb_amt_6m']
        self.total_loan_amt_cnt['loan_amt_growth_3m_0m'] = (self.total_loan_amt_cnt['disb_amt_3m']) / (self.total_loan_amt_cnt['disb_loan_amt_ever'] - self.total_loan_amt_cnt['disb_amt_3m'])

        self.total_loan_amt_cnt['loan_count_growth_18m_12m'] = (self.total_loan_amt_cnt['disb_cnt_18m'] - self.total_loan_amt_cnt['disb_cnt_12m']) / self.total_loan_amt_cnt['disb_cnt_18m']
        self.total_loan_amt_cnt['loan_count_growth_12m_6m'] = (self.total_loan_amt_cnt['disb_cnt_12m'] - self.total_loan_amt_cnt['disb_cnt_6m']) / self.total_loan_amt_cnt['disb_cnt_12m']
        self.total_loan_amt_cnt['loan_count_growth_6m_3m'] = (self.total_loan_amt_cnt['disb_cnt_6m'] - self.total_loan_amt_cnt['disb_cnt_3m']) / self.total_loan_amt_cnt['disb_cnt_6m']
        self.total_loan_amt_cnt['loan_count_growth_3m_0m'] = (self.total_loan_amt_cnt['disb_cnt_3m']) / (self.total_loan_amt_cnt['disb_loan_cnt_ever'] - self.total_loan_amt_cnt['disb_cnt_3m'])

        self.total_loan_amt_cnt['loan_amt_growth_18m_12m'] = self.total_loan_amt_cnt['disb_amt_12m'] / (self.total_loan_amt_cnt['disb_amt_18m'] - self.total_loan_amt_cnt['disb_amt_12m'])
        self.total_loan_amt_cnt['loan_amt_growth_12m_6m'] = self.total_loan_amt_cnt['disb_amt_6m'] / (self.total_loan_amt_cnt['disb_amt_12m'] - self.total_loan_amt_cnt['disb_amt_6m'])
        self.total_loan_amt_cnt['loan_amt_growth_6m_3m'] = self.total_loan_amt_cnt['disb_amt_3m'] / (self.total_loan_amt_cnt['disb_amt_6m'] - self.total_loan_amt_cnt['disb_amt_3m'])
        self.total_loan_amt_cnt['loan_amt_growth_3m_0m'] = (self.total_loan_amt_cnt['disb_amt_3m']) / (self.total_loan_amt_cnt['disb_loan_amt_ever'] - self.total_loan_amt_cnt['disb_amt_3m'])

        self.total_loan_amt_cnt['loan_count_growth_18m_12m'] = self.total_loan_amt_cnt['disb_cnt_12m'] / (self.total_loan_amt_cnt['disb_cnt_18m'] - self.total_loan_amt_cnt['disb_cnt_12m'])
        self.total_loan_amt_cnt['loan_count_growth_12m_6m'] =  self.total_loan_amt_cnt['disb_cnt_6m'] / (self.total_loan_amt_cnt['disb_cnt_12m'] - self.total_loan_amt_cnt['disb_cnt_6m'])
        self.total_loan_amt_cnt['loan_count_growth_6m_3m'] =  self.total_loan_amt_cnt['disb_cnt_3m'] / (self.total_loan_amt_cnt['disb_cnt_6m'] - self.total_loan_amt_cnt['disb_cnt_3m'])
        self.total_loan_amt_cnt['loan_count_growth_3m_0m'] =  self.total_loan_amt_cnt['disb_cnt_3m'] / (self.total_loan_amt_cnt['disb_loan_cnt_ever'] - self.total_loan_amt_cnt['disb_cnt_3m'])

        return

    def total_calc_writtenoff_cnt(self, cibil_df):

        app_data_wof_count = cibil_df[cibil_df['writtenoff_flag'].isin(writtenof_status)].groupby(['appid']).agg({'writtenoff_flag': func.count_nonzero_nonnan,'writtenoff_amt_calc': np.sum}).reset_index()
        app_data_wof_count.columns = ['appid', 'writtenoff_cnt_ever','writtenoff_amt_ever']

        # app_data_wof_amt = cibil_df[cibil_df['writtenoff_flag'].isin(writtenof_status)].groupby(['appid']).agg({'writtenoff_amt_calc': np.sum}).reset_index()
        # app_data_wof_amt.columns = ['appid', 'writtenoff_amt_ever']

        app_data_wof_count_3m = cibil_df[((cibil_df['time_since_reported'] <= 90) & (cibil_df['writtenoff_flag'].isin(writtenof_status)))].groupby(['appid']).agg({'writtenoff_flag': func.count_nonzero_nonnan,'writtenoff_amt_calc': np.sum}).reset_index()
        app_data_wof_count_3m.columns = ['appid', 'writtenoff_cnt_3m','writtenoff_amt_3m']

        # app_data_wof_amt_3m = cibil_df[((cibil_df['time_since_reported'] <= 90) & (cibil_df['writtenoff_flag'].isin(writtenof_status)))].groupby(['appid']).agg({'writtenoff_amt_calc': np.sum}).reset_index()
        # app_data_wof_amt_3m.columns = ['appid', 'writtenoff_amt_3m']

        app_data_wof_count_6m = cibil_df[((cibil_df['time_since_reported'] <= 180) & (cibil_df['writtenoff_flag'].isin(writtenof_status)))].groupby(['appid']).agg({'writtenoff_flag': func.count_nonzero_nonnan,'writtenoff_amt_calc': np.sum}).reset_index()
        app_data_wof_count_6m.columns = ['appid', 'writtenoff_cnt_6m', 'writtenoff_amt_6m']
        # app_data_wof_count_6m = pd.DataFrame(app_data_wof_count_6m, columns=['appid', 'writtenoff_cnt_6m'])

        # app_data_wof_amt_6m = cibil_df[((cibil_df['time_since_reported'] <= 180) & (cibil_df['writtenoff_flag'].isin(writtenof_status)))].groupby(['appid']).agg({'writtenoff_amt_calc': np.sum}).reset_index()
        # app_data_wof_amt_6m.columns = ['appid', 'writtenoff_amt_6m']

        app_data_wof_count_9m = cibil_df[((cibil_df['time_since_reported'] <= 270) & (cibil_df['writtenoff_flag'].isin(writtenof_status)))].groupby(['appid']).agg({'writtenoff_flag': func.count_nonzero_nonnan,'writtenoff_amt_calc': np.sum}).reset_index()
        # app_data_wof_count_9m = pd.DataFrame(app_data_wof_count_9m, columns=['appid', 'writtenoff_cnt_9m'])
        app_data_wof_count_9m.columns = ['appid', 'writtenoff_cnt_9m', 'writtenoff_amt_9m']

        # app_data_wof_amt_9m = cibil_df[((cibil_df['time_since_reported'] <= 270) & (cibil_df['writtenoff_flag'].isin(writtenof_status)))].groupby(['appid']).agg({'writtenoff_amt_calc': np.sum}).reset_index()
        # app_data_wof_amt_9m.columns = ['appid', 'writtenoff_amt_9m']

        app_data_wof_count_12m = cibil_df[((cibil_df['time_since_reported'] <= 365) & (cibil_df['writtenoff_flag'].isin(writtenof_status)))].groupby(['appid']).agg({'writtenoff_flag': func.count_nonzero_nonnan,'writtenoff_amt_calc': np.sum}).reset_index()
        # app_data_wof_count_12m = pd.DataFrame(app_data_wof_count_12m, columns=['appid', 'writtenoff_cnt_12m'])
        app_data_wof_count_12m.columns = ['appid', 'writtenoff_cnt_12m', 'writtenoff_amt_12m']

        # app_data_wof_amt_12m = cibil_df[((cibil_df['time_since_reported'] <= 365) & (cibil_df['writtenoff_flag'].isin(writtenof_status)))].groupby(['appid']).agg({'writtenoff_amt_calc': np.sum}).reset_index()
        # app_data_wof_amt_12m.columns = ['appid', 'writtenoff_amt_12m']

        app_data_wof_count_24m = cibil_df[((cibil_df['time_since_reported'] <= 730) & (cibil_df['writtenoff_flag'].isin(writtenof_status)))].groupby(['appid']).agg({'writtenoff_flag': func.count_nonzero_nonnan,'writtenoff_amt_calc': np.sum}).reset_index()
        # app_data_wof_count_24m = pd.DataFrame(app_data_wof_count_24m, columns=['appid', 'writtenoff_cnt_24m'])
        app_data_wof_count_24m.columns = ['appid', 'writtenoff_cnt_24m', 'writtenoff_amt_24m']
        #
        # app_data_wof_amt_24m = cibil_df[((cibil_df['time_since_reported'] <= 730) & (cibil_df['writtenoff_flag'].isin(writtenof_status)))].groupby(['appid']).agg({'writtenoff_amt_calc': np.sum}).reset_index()
        # app_data_wof_amt_24m.columns = ['appid', 'writtenoff_amt_24m']

        # app_data_wof_count_36m = cibil_df[cibil_df['time_since_reported'] <= 1080].groupby(['appid']).agg({'writtenoff_flag': func.count_nonzero_nonnan}).reset_index()
        # app_data_wof_count_36m = pd.DataFrame(app_data_wof_count_36m, columns=['appid', 'writtenoff_cnt_36m'])

        # app_data_wof_amt_36m = cibil_df[((cibil_df['time_since_reported'] <= 1080) & (~pd.isnull(cibil_df['writtenoff_flag'])))].groupby(['appid']).agg({'writtenoff_amt_calc': np.sum}).reset_index()
        # app_data_wof_amt_36m.columns = ['appid', 'writtenoff_amt_36m']
        #
        # app_data_wof_count_48m = cibil_df[cibil_df['time_since_reported'] <= 1440].groupby(['appid']).agg({'writtenoff_flag': func.count_nonzero_nonnan}).reset_index()
        # app_data_wof_count_48m = pd.DataFrame(app_data_wof_count_48m, columns=['appid', 'writtenoff_cnt_48m'])
        #
        # app_data_wof_amt_48m = cibil_df[((cibil_df['time_since_reported'] <= 1440) & (~pd.isnull(cibil_df['writtenoff_flag'])))].groupby(['appid']).agg({'writtenoff_amt_calc': np.sum}).reset_index()
        # app_data_wof_amt_48m.columns = ['appid', 'writtenoff_amt_48m']

        dfs = [app_data_wof_count,app_data_wof_count_3m, app_data_wof_count_6m, app_data_wof_count_12m, app_data_wof_count_9m, app_data_wof_count_24m] #app_data_wof_count_36m, app_data_wof_amt_36m,app_data_wof_count_48m, app_data_wof_amt_48m

        app_data_wof_amt_cnt = reduce(lambda left, right: pd.merge(left, right, on=['appid'], how='outer'), dfs)

        app_data_wof_amt_cnt['writtenoff_amt_ever'] = app_data_wof_amt_cnt['writtenoff_amt_ever'].fillna(0)
        app_data_wof_amt_cnt['writtenoff_amt_3m'] = np.where(app_data_wof_amt_cnt['writtenoff_cnt_3m'] == 0,0,app_data_wof_amt_cnt['writtenoff_amt_3m'])
        app_data_wof_amt_cnt['writtenoff_amt_6m'] = np.where(app_data_wof_amt_cnt['writtenoff_cnt_6m'] == 0, 0, app_data_wof_amt_cnt['writtenoff_amt_6m'])
        app_data_wof_amt_cnt['writtenoff_amt_9m'] = np.where(app_data_wof_amt_cnt['writtenoff_cnt_9m'] == 0, 0, app_data_wof_amt_cnt['writtenoff_amt_9m'])
        app_data_wof_amt_cnt['writtenoff_amt_12m'] = np.where(app_data_wof_amt_cnt['writtenoff_cnt_12m'] == 0, 0, app_data_wof_amt_cnt['writtenoff_amt_12m'])
        app_data_wof_amt_cnt['writtenoff_amt_24m'] = np.where(app_data_wof_amt_cnt['writtenoff_cnt_24m'] == 0, 0, app_data_wof_amt_cnt['writtenoff_amt_24m'])
        # app_data_wof_amt_cnt['writtenoff_amt_36m'] = np.where(app_data_wof_amt_cnt['writtenoff_cnt_36m'] == 0, 0, app_data_wof_amt_cnt['writtenoff_amt_36m'])
        # app_data_wof_amt_cnt['writtenoff_amt_48m'] = np.where(app_data_wof_amt_cnt['writtenoff_cnt_48m'] == 0, 0, app_data_wof_amt_cnt['writtenoff_amt_48m'])

        # app_data_wof_amt_cnt = app_data_wof_amt_cnt.fillna(0)

        return app_data_wof_amt_cnt

    def total_calc_max_dpd(self,cibil_df):

        max_dpd_ever = cibil_df.groupby(['appid']).agg({'max_dpd': np.max}).reset_index()
        max_dpd_ever.columns = ['appid', 'max_dpd_ever']

        max_dpd_3m = cibil_df.groupby(['appid']).agg({'max_dpd_3m': np.max}).reset_index()
        max_dpd_3m.columns = ['appid',  'max_dpd_3m']

        max_dpd_6m = cibil_df.groupby(['appid']).agg({'max_dpd_6m': np.max}).reset_index()
        max_dpd_6m.columns = ['appid', 'max_dpd_6m']

        max_dpd_9m = cibil_df.groupby(['appid']).agg({'max_dpd_9m': np.max}).reset_index()
        max_dpd_9m.columns = ['appid', 'max_dpd_9m']

        max_dpd_12m = cibil_df.groupby(['appid']).agg({'max_dpd_12m': np.max}).reset_index()
        max_dpd_12m.columns = ['appid', 'max_dpd_12m']

        max_dpd_24m = cibil_df.groupby(['appid']).agg({'max_dpd_24m': np.max}).reset_index()
        max_dpd_24m.columns = ['appid', 'max_dpd_24m']

        # max_dpd_36m = cibil_df.groupby(['appid']).agg({'max_dpd_36m': np.max}).reset_index()
        # max_dpd_36m.columns = ['appid', 'max_dpd_36m']
        #
        # max_dpd_48m = cibil_df.groupby(['appid']).agg({'max_dpd_48m': np.max}).reset_index()
        # max_dpd_48m.columns = ['appid', 'max_dpd_48m']

        cibil_df_close = cibil_df[~pd.isnull(cibil_df['new_dateClosed'])]
        cibil_df_open = cibil_df[pd.isnull(cibil_df['new_dateClosed'])]
        #########-------- For Closed Loans ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        max_dpd_close_ever = cibil_df_close.groupby(['appid']).agg({'max_dpd': np.max}).reset_index()
        max_dpd_close_ever.columns = ['appid','max_dpd_close_ever']

        max_dpd_close_3m = cibil_df_close.groupby(['appid']).agg({'max_dpd_3m': np.max}).reset_index()
        max_dpd_close_3m.columns = ['appid', 'max_dpd_close_3m']

        max_dpd_close_6m = cibil_df_close.groupby(['appid']).agg({'max_dpd_6m': np.max}).reset_index()
        max_dpd_close_6m.columns = ['appid', 'max_dpd_close_6m']

        max_dpd_close_9m = cibil_df_close.groupby(['appid']).agg({'max_dpd_9m': np.max}).reset_index()
        max_dpd_close_9m.columns = ['appid', 'max_dpd_close_9m']

        max_dpd_close_12m = cibil_df_close.groupby(['appid']).agg({'max_dpd_12m': np.max}).reset_index()
        max_dpd_close_12m.columns = ['appid',  'max_dpd_close_12m']

        max_dpd_close_24m = cibil_df_close.groupby(['appid']).agg({'max_dpd_24m': np.max}).reset_index()
        max_dpd_close_24m.columns = ['appid', 'max_dpd_close_24m']
        #
        # max_dpd_close_36m = cibil_df_close.groupby(['appid']).agg({'max_dpd_36m': np.max}).reset_index()
        # max_dpd_close_36m.columns = ['appid', 'max_dpd_close_36m']
        #
        # max_dpd_close_48m = cibil_df_close.groupby(['appid']).agg({'max_dpd_48m': np.max}).reset_index()
        # max_dpd_close_48m.columns = ['appid', 'max_dpd_close_48m']

        #########-------- For Open Loans ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        max_dpd_open_ever = cibil_df_open.groupby(['appid']).agg({'max_dpd': np.max}).reset_index()
        max_dpd_open_ever.columns = ['appid',  'max_dpd_open_ever']

        max_dpd_open_3m = cibil_df_open.groupby(['appid']).agg({'max_dpd_3m': np.max}).reset_index()
        max_dpd_open_3m.columns = ['appid', 'max_dpd_open_3m']

        max_dpd_open_6m = cibil_df_open.groupby(['appid']).agg({'max_dpd_6m': np.max}).reset_index()
        max_dpd_open_6m.columns = ['appid',  'max_dpd_open_6m']

        max_dpd_open_9m = cibil_df_open.groupby(['appid']).agg({'max_dpd_9m': np.max}).reset_index()
        max_dpd_open_9m.columns = ['appid', 'max_dpd_open_9m']

        max_dpd_open_12m = cibil_df_open.groupby(['appid']).agg({'max_dpd_12m': np.max}).reset_index()
        max_dpd_open_12m.columns = ['appid', 'max_dpd_open_12m']

        max_dpd_open_24m = cibil_df_open.groupby(['appid']).agg({'max_dpd_24m': np.max}).reset_index()
        max_dpd_open_24m.columns = ['appid', 'max_dpd_open_24m']
        #
        # max_dpd_open_36m = cibil_df_open.groupby(['appid']).agg({'max_dpd_36m': np.max}).reset_index()
        # max_dpd_open_36m.columns = ['appid', 'max_dpd_open_36m']
        #
        # max_dpd_open_48m = cibil_df_open.groupby(['appid']).agg({'max_dpd_48m': np.max}).reset_index()
        # max_dpd_open_48m.columns = ['appid', 'max_dpd_open_48m']


        dfs = [max_dpd_ever,max_dpd_3m,max_dpd_6m,max_dpd_9m,max_dpd_12m,max_dpd_24m,max_dpd_close_ever, max_dpd_close_3m,
               max_dpd_close_6m, max_dpd_close_9m,max_dpd_close_12m,max_dpd_close_24m, max_dpd_open_ever, max_dpd_open_3m,
               max_dpd_open_6m,max_dpd_open_9m, max_dpd_open_12m,max_dpd_open_24m]
        #,max_dpd_36m,max_dpd_48m,max_dpd_close_36m,max_dpd_close_48m,max_dpd_open_36m,max_dpd_open_48m
        max_dpd_open_close = reduce(lambda left, right: pd.merge(left, right, on=['appid'], how='outer'), dfs)

        max_dpd_open_close['max_dpd_3m_to_6m'] = max_dpd_open_close.apply(lambda x: np.nan if x['max_dpd_6m'] == 0 else float(x['max_dpd_3m']) / x['max_dpd_6m'], axis=1)
        max_dpd_open_close['max_dpd_6m_to_12m'] = max_dpd_open_close.apply(lambda x: np.nan if x['max_dpd_12m'] == 0 else float(x['max_dpd_6m']) / x['max_dpd_12m'], axis=1)
        max_dpd_open_close['max_dpd_12m_to_24m'] = max_dpd_open_close.apply(lambda x: np.nan if x['max_dpd_24m'] == 0 else float(x['max_dpd_12m']) / x['max_dpd_24m'], axis=1)

        return max_dpd_open_close

    def total_calc_delay_emi_cnts(self,cibil_df):
        emi_cnt_perc = cibil_df[['appid']].drop_duplicates()
        for delay in delinquencies:
            for mo in ['ever', '3m', '6m', '9m', '12m', '24m']:
                cnt_col = 'emi_count_' + str(mo) + "_" + str(delay)
                perc_col = 'emi_count_' + str(mo) + "_" + str(delay) + "plus_perc"
                my_df = cibil_df.groupby(['appid']).agg({perc_col: np.mean, cnt_col: np.sum}).reset_index()
                emi_cnt_perc = pd.merge(emi_cnt_perc, my_df, on=['appid'], how='outer')

        acct_agg_perc = self.total_calc_delay_loan_cnts(cibil_df)

        final_perc_calc = pd.merge(emi_cnt_perc,acct_agg_perc, on=['appid'], how='outer')

        for delay in delinquencies:
            for mo in ['ever', '3m', '6m', '9m', '12m', '24m']:
                num_col = 'emi_count_' + str(mo) + "_" + str(delay)
                # den_col = 'accts_'+str(delay)+"plus_"+str(mo)
                if mo != 'ever':
                    den_col = 'acct_for_' + str(mo)
                else:
                    den_col = 'total_accts'
                # final_perc_calc['emi_count_per_acct_'+str(mo)+"_"+str(delay)+"plus_perc"] = final_perc_calc.apply(lambda x: np.nan if x[den_col]==0 else float(x[num_col])/x[den_col],axis=1)
                final_perc_calc['emi_count_per_acct_'+str(mo)+"_"+str(delay)+"plus_perc"] = np.where(final_perc_calc[den_col]==0,np.nan,final_perc_calc[num_col]/final_perc_calc[den_col]*1.0)

        return final_perc_calc


    def total_calc_delay_loan_cnts(self,cibil_df):
        agg_act_cnt = cibil_df[['appid']].drop_duplicates()
        agg_acct_delay_cnt = cibil_df[['appid']].drop_duplicates()

        """ Denominator """
        for mo in ['ever','1m','3m','6m','9m','12m','24m']:
            if mo != 'ever':
                col = 'acct_for_' + str(mo)
            else:
                col = 'total_accts'
            my_df = cibil_df.groupby(['appid']).agg({col:np.sum}).reset_index()
            agg_act_cnt = pd.merge(agg_act_cnt,my_df,on=['appid'],how='outer')

        for delay in delinquencies:
            for mo in ['ever','1m','3m','6m','9m','12m','24m']:
                col = 'accts_' + str(delay)+ "plus_"+ str(mo)
                my_df = cibil_df.groupby(['appid']).agg({col:np.sum}).reset_index()
                agg_acct_delay_cnt = pd.merge(agg_acct_delay_cnt,my_df,on=['appid'],how='outer')

        final_agg = pd.merge(agg_act_cnt,agg_acct_delay_cnt,on=['appid'],how='inner')

        for delay in delinquencies:
            for mo in ['ever','1m','3m','6m','9m','12m','24m']:
                num_col = 'accts_' + str(delay)+ "plus_"+ str(mo)

                if mo != 'ever':
                    den_col = 'acct_for_' + str(mo)
                else:
                    den_col = 'total_accts'

                # final_agg[num_col+"_perc"] = final_agg.apply(lambda x: np.nan if x[den_col]==0 else float(x[num_col])/x[den_col],axis=1)
                final_agg[num_col+"_perc"] = np.where(final_agg[den_col]==0,np.nan,final_agg[num_col]/final_agg[den_col]*1.0)


        return final_agg

    def total_current_outstanding_balance(self,cibil_df):

        outstanding_bal = cibil_df.groupby(['appid']).agg({'current_os_bal':np.sum,'current_os_bal_30plus':np.sum,'current_os_bal_60plus':np.sum,'current_os_bal_90plus':np.sum}).reset_index()
        outstanding_bal['os_30plus_perc'] = np.where(outstanding_bal['current_os_bal']==0,np.nan,outstanding_bal['current_os_bal_30plus']/outstanding_bal['current_os_bal'])
        outstanding_bal['os_60plus_perc'] = np.where(outstanding_bal['current_os_bal']==0,np.nan,outstanding_bal['current_os_bal_60plus']/outstanding_bal['current_os_bal'])
        outstanding_bal['os_90plus_perc'] = np.where(outstanding_bal['current_os_bal']==0,np.nan,outstanding_bal['current_os_bal_90plus']/outstanding_bal['current_os_bal'])
        return outstanding_bal

    def total_youngest_loan(self, cibil_df):
        youngest_loan = cibil_df.groupby(['appid']).agg({'dateopened': np.max}).reset_index()
        youngest_loan = pd.merge(youngest_loan, cibil_df[['appid', 'created_time']].drop_duplicates(), on='appid', how='left')
        youngest_loan['youngest_loan'] = round((youngest_loan['created_time'] - youngest_loan['dateopened']).dt.days/30, 0)
        # youngest_loan['CREATED_TIME'] = youngest_loan.apply(lambda x: cibil_df.loc[cibil_df['appid'] == x['appid'],'CREATED_TIME'].iloc[0], axis=1)
        # youngest_loan['youngest_loan'] = youngest_loan.apply(lambda x: int((x['CREATED_TIME'] - x['dateOpened_Disbursed']).days/30), axis=1)
        return youngest_loan[['appid', 'youngest_loan']]

    def total_oldest_loan(self, cibil_df):
        oldest_loan = cibil_df.groupby(['appid']).agg({'dateopened': np.min}).reset_index()
        oldest_loan = pd.merge(oldest_loan, cibil_df[['appid', 'created_time']].drop_duplicates(), on='appid', how='left')
        oldest_loan['oldest_loan'] = round((oldest_loan['created_time'] - oldest_loan['dateopened']).dt.days / 30, 0)

        # oldest_loan['CREATED_TIME'] = oldest_loan.apply(lambda x: cibil_df.loc[cibil_df['appid'] == x['appid'],'CREATED_TIME'].iloc[0], axis=1)
        # oldest_loan['oldest_loan'] = oldest_loan.apply(lambda x: int((x['CREATED_TIME'] - x['dateOpened_Disbursed']).days/30), axis=1)
        return oldest_loan[['appid', 'oldest_loan']]

    def total_delay_amount(self, cibil_df):
        filtered_df = cibil_df.query("M00 >=7 and dateClosed == 'NaT'")
        delayed_df = filtered_df.groupby(['appid']).agg({'current_os_bal': np.sum, 'appid': [func.count_nonzero_nonnan]}).reset_index()
        delayed_df.columns = ['appid', 'delay_amount', 'count_of_delay_loans']
        return delayed_df

    def new_total_last_delinq(self,cibil_df):
        cibil_df['tag'] = cibil_df[var_list].fillna(0).ge(7.0).T.idxmax() #"""To get first column name of paymentstirngs where it goes above 7 DPD, if its not there then it will return M00'
        cibil_df['tag'] = cibil_df.apply(lambda x: np.nan if x[x['tag']] == 0 else x['tag'],axis=1)
        cibil_df['new_tag'] = np.where(pd.isnull(cibil_df['tag']),np.nan,cibil_df['tag'].astype(str)+"_phd")
        cibil_df['new_tag_val'] = cibil_df.apply(lambda x: np.nan if pd.isnull(x['new_tag']) else x[x['new_tag']],axis=1)
        agg_df = cibil_df.groupby('appid')['new_tag_val'].apply(list).reset_index(name='new')
        agg_df['new'] = agg_df.apply(lambda x: [i for i in x['new'] if i == i],axis=1)
        agg_df['min_phd_date'] = agg_df['new'].apply(lambda x: np.nan if len(x)==0 else max(x))
        agg_df = pd.merge(agg_df,cibil_df[['appid','CREATED_TIME']].drop_duplicates(),on='appid',how='left')
        agg_df['month_since_last_delinquency'] = agg_df.apply(lambda x: int((x['CREATED_TIME'] - x['min_phd_date']).days / 30) if (not pd.isnull(x['min_phd_date'])) else np.nan, axis=1)
        # agg_df['month_since_last_delinquency'] = np.where(agg_df['min_phd_date']=='',np.nan,(agg_df['CREATED_TIME'] - agg_df['min_phd_date']).dt.days / 30.0)

        return agg_df[['appid','month_since_last_delinquency']]

    def credit_card_analysis(self,cibil_df):
        credit_card_data = cibil_df[['appid', 'accounttype', 'creditlimit', 'sanctionamount','balance','dateclosed']]
        credit_card_data = credit_card_data[(credit_card_data['accounttype'].str.contains(r'Credit Card',regex=True))]

        if len(credit_card_data) > 0:

            credit_card_data = credit_card_data.dropna(subset=['creditlimit', 'sanctionamount'], how='all')
            credit_card_data = credit_card_data[(pd.isnull(credit_card_data['dateclosed']))]

            if len(credit_card_data) > 0:
                credit_card_data['creditlimit'] = credit_card_data[['creditlimit', 'sanctionamount']].max(axis=1)
                credit_agg = credit_card_data.groupby(['appid'])['creditlimit', 'sanctionamount', 'balance'].sum().reset_index()
                credit_agg.index.name = "Key"

                credit_agg['credit_usage_ratio'] = np.where(credit_agg['creditlimit'] == 0, np.nan, credit_agg['balance']/credit_agg['creditlimit'])

                # try:
                #     credit_agg['Ratio'] = credit_agg.apply(lambda x: (float(x['currentBalance']) / x['creditLimit']), axis=1)
                # except:
                #     credit_agg['Ratio'] = np.nan
                #
                # credit_agg = credit_agg.rename(columns = {'Ratio':'credit_usage_ratio'})
            else:
                credit_agg = pd.DataFrame()
                credit_agg = credit_agg.reindex(columns=['appid'])
        else:
            credit_agg = pd.DataFrame()
            credit_agg = credit_agg.reindex(columns=['appid'])
        # print("7." + str(len(credit_agg['appid'].unique())))
        if 'sanctionamount' in credit_agg.columns:
            del credit_agg['sanctionamount']
        return credit_agg


    def overdraft_analysis(self,cibil_df):
        od_data = cibil_df[['appid', 'accounttype', 'sanctionamount', 'balance', 'dateclosed']]
        od_data = od_data[od_data['accounttype'] == 'Overdraft']

        if len(od_data) > 0:

            od_data = od_data[pd.isnull(od_data['dateclosed'])]

            if len(od_data) > 0 :
                od_agg = od_data.groupby(['appid'])['balance', 'sanctionamount'].sum().reset_index()
                od_agg.index.name = "Key"
                od_agg['Ratio'] = np.where(od_agg['sanctionamount']==0,np.nan,od_agg['balance'] / od_agg['sanctionamount'])
                # try:
                #     od_agg['Ratio'] = od_agg.apply(lambda x: (float(x['currentBalance']) / x['sanctionamount']), axis=1)
                # except:
                #     od_agg['Ratio'] = np.nan
                od_agg = od_agg.rename(columns={'Ratio': 'od_usage_ratio', 'balance': 'od_used_amt'})
            else:
                od_agg = pd.DataFrame()
                od_agg = od_agg.reindex(columns=['appid'])
        else:
            od_agg = pd.DataFrame()
            od_agg = od_agg.reindex(columns=['appid'])
        # print("7." + str(len(credit_agg['appid'].unique())))
        if 'sanctionamount' in od_agg.columns:
            del od_agg['sanctionamount']
        return od_agg

    # def total_last_deliquence(self, cibil_df):
    #     temp_df = cibil_df.copy()
    #     vars = copy.deepcopy(var_list)
    #     vars.append('appid')
    #     vars.append('accountType')
    #     vars.extend(phd_var_list)
    #     filtered_df = temp_df[vars]
    #     filtered_df = filtered_df.fillna('0')
    #     output_list = []
    #     output_dict = {}
    #     for key, value in filtered_df.iterrows():
    #         appid = value['appid']
    #         key = appid
    #         if key not in output_dict.keys():
    #             output_dict[key] = []
    #         for v in range(0, (len(vars) - len(phd_var_list) - 2)):
    #             dpd_value = value[vars[v]]
    #             if float(dpd_value) >= 7.0:
    #                 output_dict[key].append(value[vars[v] + '_phd'])
    #                 break
    #     for od_key, od_value in output_dict.items():
    #         if len(od_value) > 0:
    #             output_dict[od_key] = max(od_value)
    #         else:
    #             output_dict[od_key] = ''
    #     output_list.append(output_dict)
    #     output_df = pd.DataFrame(output_list).T.reset_index()
    #     output_df.columns = ['appid', 'min_phd_date']
    #     # print (key, value)
    #     # for v in range(0, len(vars) - 2):
    #     #     filtered_df[vars[v]] = filtered_df.apply(lambda x: (v + 1) if int(x[vars[v]]) != 0 else 999, axis=1)
    #     # filtered_df['min'] = filtered_df[[('M0' + str(i)) if i < 10 else ('M' + str(i)) for i in range(36)]].apply(lambda x: x.min(), axis=1)
    #     # filtered_df = filtered_df.groupby(['appid', 'accountType']).agg({'min': np.min}).reset_index()
    #     # filtered_df.columns = ['appid', 'accountType', 'last_dpd']
    #     output_df['CREATED_TIME'] = output_df.apply(lambda x: temp_df.loc[temp_df['appid'] == x['appid'],'CREATED_TIME'].iloc[0], axis=1)
    #     #
    #     # # filtered_df['min_phd_date'] = filtered_df.apply(lambda x: temp_df.loc[(temp_df['appid'] == x['appid']) &
    #     #                                                                       (temp_df['accountType'] == x['accountType']) &
    #     #                                                                       # (temp_df['M'+str(int(x['last_dpd'])-1).zfill(2)] != 'nan') &
    #     #                                                                       # (not pd.isna(temp_df['M'+str(int(x['last_dpd'])-1).zfill(2)])) &
    #     #                                                                       (temp_df['M'+str(int(x['last_dpd'])-1).zfill(2)] != 0),
    #     #                                                                       'M'+str(int(x['last_dpd'])-1).zfill(2)+'_phd'].dropna().iloc[0] if x['last_dpd'] != 999 else '', axis=1)
    #     output_df['min_phd_date'] = pd.to_datetime(output_df['min_phd_date'], dayfirst=True)
    #     output_df['month_since_last_delinquency'] = output_df.apply(lambda x: int((x['CREATED_TIME'] - x['min_phd_date']).days / 30) if (not pd.isnull(x['min_phd_date'])) else '', axis=1)
    #     # del filtered_df['last_dpd']
    #     del output_df['CREATED_TIME']
    #     del output_df['min_phd_date']
    #     return output_df

    def total_dpd_ratio(self, cibil_df):
        total_dpd_df = cibil_df.groupby(['appid', 'dpd_range']).agg({'accountstatus': np.count_nonzero}).reset_index()
        total_dpd_df = total_dpd_df.pivot(index='appid', columns='dpd_range', values='accountstatus').reset_index()

        app_data_amt_count_ever = cibil_df.groupby(['appid']).agg({'sanctionamount': func.count_nonzero_nonnan}).reset_index()
        app_data_amt_count_ever.columns = ['appid', 'disb_loan_cnt_ever']
        total_dpd_df = pd.merge(app_data_amt_count_ever, total_dpd_df, how='left', on='appid')
        for dpd in dpd_var_list:
            if dpd not in total_dpd_df.columns:
                total_dpd_df[dpd] = np.nan
            total_dpd_df[dpd + '_ratio'] = total_dpd_df.apply(lambda x: x[dpd] / x['disb_loan_cnt_ever'] if not pd.isna(x[dpd]) else np.nan, axis=1)
        del total_dpd_df['disb_loan_cnt_ever']
        return total_dpd_df

    def bureau_thick_bad_calc(self):
        self.total_cibil_loans['no_of_dpd_count'] = self.total_cibil_loans[['90-119_ratio', '120-179_ratio', '180-359_ratio', '360-539_ratio', '540-719_ratio', '720+_ratio']].count(axis=1)
        self.total_cibil_loans['bureau_thick_bad'] = self.total_cibil_loans[['no_of_dpd_count', 'restructure_count', 'written_off_count']].sum(axis=1)
        return

    def impute_total_income(self, cibil_df):
        filter_df = cibil_df[cibil_df['time_since_reported'] < 720]
        # filter_df['projected_emi'] = filter_df.apply(lambda x: float(x['weightage_of_sanctioned_amount']) * (float(x['sanctionamount']) if x['accounttype'] == 'Credit Card' else float(x['sanctionamount'])), axis=1)
        filter_df['projected_emi'] = filter_df.apply(lambda x: float(x['weightage_of_sanctioned_amount']) * (float(x['creditlimit_for_income']) if x['accounttype'] == 'Credit Card' else float(x['sanctionamount'])), axis=1)
        filter_df['weighted_emi'] = filter_df.apply(lambda x: x['projected_emi'] * (1 if x['ownershiptype'] == 'Individual' else 0.5), axis=1)

        dates_df = cibil_df[['appid', 'created_time']].drop_duplicates().reset_index(drop=True)
        dates_df[date_list_for_emi] = pd.DataFrame(dates_df.apply(lambda x: list(pd.date_range(end=x['created_time'], periods=24, freq='M')), axis=1).apply(list).tolist())
        filter_df = pd.merge(filter_df, dates_df, on=['appid', 'created_time'], how='left')

        melt_df = pd.melt(filter_df, id_vars=['appid'], value_vars=date_list_for_emi).drop_duplicates().reset_index(drop=True)
        for index, row in melt_df.iterrows():
            data_df = filter_df[(filter_df['appid'] == row['appid']) & (filter_df['dateopened'] <= row['value'])]
            hl_df = data_df[data_df['accounttype'] == 'Housing Loan']
            cc_df = data_df[data_df['accounttype'] == 'Credit Card']
            bl_df = data_df[(data_df['accounttype'] == 'Business Loan') & (data_df['sanctionamount'] > 50000)]
            pl_df = data_df[(data_df['accounttype'] == 'Personal Loan') & (data_df['sanctionamount'] > 50000)]
            if data_df.empty:
                melt_df.loc[index, 'emi_amount'] = 0.00
                melt_df.loc[index, 'hl_emi_amount'] = 0.00
                melt_df.loc[index, 'cc_emi_amount'] = 0.00
                melt_df.loc[index, 'bl_emi_amount'] = 0.00
                melt_df.loc[index, 'pl_emi_amount'] = 0.00
            else:
                melt_df.loc[index, 'emi_amount'] = data_df.groupby(['appid']).agg({'weighted_emi': np.sum}).reset_index()['weighted_emi'][0]
                if hl_df.empty:
                    melt_df.loc[index, 'hl_emi_amount'] = 0.00
                else:
                    melt_df.loc[index, 'hl_emi_amount'] = hl_df.groupby(['appid']).agg({'weighted_emi': np.sum}).reset_index()['weighted_emi'][0]
                if cc_df.empty:
                    melt_df.loc[index, 'cc_emi_amount'] = 0.00
                else:
                    melt_df.loc[index, 'cc_emi_amount'] = cc_df.groupby(['appid']).agg({'creditlimit_for_income': np.max}).reset_index()['creditlimit_for_income'][0]
                    # melt_df.loc[index, 'cc_emi_amount'] = cc_df.groupby(['appid']).agg({'sanctionamount': np.max}).reset_index()['sanctionamount'][0]
                if bl_df.empty:
                    melt_df.loc[index, 'bl_emi_amount'] = 0.00
                else:
                    melt_df.loc[index, 'bl_emi_amount'] = bl_df.groupby(['appid']).agg({'weighted_emi': np.sum}).reset_index()['weighted_emi'][0]
                if pl_df.empty:
                    melt_df.loc[index, 'pl_emi_amount'] = 0.00
                else:
                    melt_df.loc[index, 'pl_emi_amount'] = pl_df.groupby(['appid']).agg({'weighted_emi': np.sum}).reset_index()['weighted_emi'][0]
        final_df = melt_df.groupby(['appid']).agg({'emi_amount': np.max, 'hl_emi_amount': np.max, 'cc_emi_amount': np.max, 'bl_emi_amount': np.max, 'pl_emi_amount': np.max}).reset_index()
        final_df['imputed_annual_income'] = final_df.apply(lambda x: max(x['emi_amount']*2.0, x['hl_emi_amount']*2.5, x['cc_emi_amount']/4.0, x['bl_emi_amount']*3.0, x['pl_emi_amount']*3.0)*12.0, axis=1)
        return final_df[['appid', 'imputed_annual_income']]
