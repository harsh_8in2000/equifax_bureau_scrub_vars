import pandas as pd
import numpy as np
from functools import reduce
from utils import util_func as func
from configs.constant import delinquencies,phd_var_list,var_list,writtenof_status
from utils.time_calculator import timeit


class CATEGORY_AGGREGATION():
    def __init__(self,dc,accountype_map):
        cibil_df_category = pd.merge(dc._acctSegCl, accountype_map, on='accounttype',how='left')
        coll_acc_list = cibil_df_category['collateralized'].dropna().unique().tolist()
        del cibil_df_category['ticket size'], cibil_df_category['collateralized']
        cibil_df_category['collateralized'] = cibil_df_category['accounttype'].apply(lambda x: 'collateralized' if str(x).lower() in coll_acc_list else 'non-collateralized')
        # del cibil_df_category['accounttype']
        # cibil_df_category.rename(columns={'category': 'accounttype'}, inplace=True)
        self.app_data_amt_count_category = self.calc_loan_amt_count(cibil_df_category)
        self.calc_growth_loan_amt_count()
        self.app_youngest_loan_df = self.youngest_loan(cibil_df_category)
        self.app_oldest_loan_df = self.oldest_loan(cibil_df_category)

        # self.outstanding_agg_category = self.current_outstanding_balance(cibil_df_category)
        # self.app_data_wof_count_category = self.calc_writtenoff_cnt(cibil_df_category)
        # self.app_max_dpd_category = self.calc_max_dpd(cibil_df_category)
        # self.app_delays_category = self.calc_delay_emi_cnts(cibil_df_category)
        # self.app_delay_amount_df = self.delay_amount(cibil_df_category)
        # self.last_deliquence_df = self.new_total_last_delinq(cibil_df_category)

        df1s = [self.app_data_amt_count_category,self.app_youngest_loan_df, self.app_oldest_loan_df] #self.last_deliquence_df
        self.cibil_loans_category = reduce(lambda left, right: pd.merge(left, right, on=['appid', 'accounttype'], how='outer'), df1s)
        self.cibil_loans_agg_data = self.pivot_processing_loan(self.cibil_loans_category)

        df2s = []
        var_list = ['business_consumer_cc']
        for var in var_list:
            del cibil_df_category['accounttype']
            cibil_df_category.rename(columns={var: 'accounttype'}, inplace=True)
            self.app_data_amt_count_category = self.calc_loan_amt_count(cibil_df_category)
            df2s.append(self.app_data_amt_count_category)

        self.cibil_loans_sub_category = reduce(lambda left, right: pd.merge(left, right, on=['appid', 'accounttype'], how='outer'), df2s)
        self.cibil_loans_sub_category_agg_data = self.pivot_processing_loan(self.cibil_loans_sub_category)

        self.cibil_loans_agg_data = pd.merge(self.cibil_loans_agg_data, self.cibil_loans_sub_category_agg_data, on='appid', how='left')

        pass

    def calc_loan_amt_count(self, cibil_df):
        app_data_amt_count_ever = cibil_df.groupby(['appid', 'accounttype']).agg({'sanctionamount': [np.sum, func.count_nonzero_nonnan, np.min, np.max]}).reset_index()
        app_data_amt_count_ever.columns = ['appid', 'accounttype', 'disb_loan_amt_ever', 'disb_loan_cnt_ever', 'min_disb_loan_amt_ever', 'max_disb_loan_amt_ever']

        app_data_amt_count_3m_since_disbursed = cibil_df[cibil_df['age_of_loan'] <= 90].groupby(['appid', 'accounttype']).agg({'sanctionamount': [np.sum, func.count_nonzero_nonnan, np.min, np.max]}).reset_index()
        app_data_amt_count_3m_since_disbursed.columns = ['appid', 'accounttype', 'disb_amt_3m', 'disb_cnt_3m', 'min_disb_amt_3m', 'max_disb_amt_3m']

        app_data_amt_count_6m_since_disbursed = cibil_df[cibil_df['age_of_loan'] <= 180].groupby(['appid', 'accounttype']).agg({'sanctionamount': [np.sum, func.count_nonzero_nonnan, np.min, np.max]}).reset_index()
        app_data_amt_count_6m_since_disbursed.columns = ['appid', 'accounttype', 'disb_amt_6m', 'disb_cnt_6m', 'min_disb_amt_6m', 'max_disb_amt_6m']

        app_data_amt_count_9m_since_disbursed = cibil_df[cibil_df['age_of_loan'] <= 270].groupby(['appid', 'accounttype']).agg({'sanctionamount': [np.sum, func.count_nonzero_nonnan, np.min, np.max]}).reset_index()
        app_data_amt_count_9m_since_disbursed.columns = ['appid', 'accounttype', 'disb_amt_9m', 'disb_cnt_9m', 'min_disb_amt_9m', 'max_disb_amt_9m']

        app_data_amt_count_12m_since_disbursed = cibil_df[cibil_df['age_of_loan'] <= 365].groupby(['appid', 'accounttype']).agg({'sanctionamount': [np.sum, func.count_nonzero_nonnan, np.min, np.max]}).reset_index()
        app_data_amt_count_12m_since_disbursed.columns = ['appid', 'accounttype', 'disb_amt_12m', 'disb_cnt_12m', 'min_disb_amt_12m', 'max_disb_amt_12m']

        app_data_amt_count_18m_since_disbursed = cibil_df[cibil_df['age_of_loan'] <= 540].groupby(['appid', 'accounttype']).agg({'sanctionamount': [np.sum, func.count_nonzero_nonnan, np.min, np.max]}).reset_index()
        app_data_amt_count_18m_since_disbursed.columns = ['appid', 'accounttype', 'disb_amt_18m', 'disb_cnt_18m', 'min_disb_amt_18m', 'max_disb_amt_18m']

        app_data_amt_count_24m_since_disbursed = cibil_df[cibil_df['age_of_loan'] <= 730].groupby(['appid', 'accounttype']).agg({'sanctionamount': [np.sum, func.count_nonzero_nonnan, np.min, np.max]}).reset_index()
        app_data_amt_count_24m_since_disbursed.columns = ['appid', 'accounttype', 'disb_amt_24m', 'disb_cnt_24m', 'min_disb_amt_24m', 'max_disb_amt_24m']

        # app_data_amt_count_36m_since_disbursed = cibil_df[cibil_df['age_of_loan'] <= 1080].groupby(['appid', 'accounttype']).agg({'sanctionamount': [np.sum, func.count_nonzero_nonnan, np.min, np.max]}).reset_index()
        # app_data_amt_count_36m_since_disbursed.columns = ['appid', 'accounttype', 'disb_amt_36m', 'disb_cnt_36m','min_disb_amt_36m','max_disb_amt_36m']
        #
        # app_data_amt_count_48m_since_disbursed = cibil_df[cibil_df['age_of_loan'] <= 1440].groupby(['appid', 'accounttype']).agg({'sanctionamount': [np.sum, func.count_nonzero_nonnan, np.min, np.max]}).reset_index()
        # app_data_amt_count_48m_since_disbursed.columns = ['appid', 'accounttype', 'disb_amt_48m', 'disb_cnt_48m','min_disb_amt_48m','max_disb_amt_48m']

        dfs = [app_data_amt_count_3m_since_disbursed, app_data_amt_count_6m_since_disbursed, app_data_amt_count_9m_since_disbursed, app_data_amt_count_12m_since_disbursed, app_data_amt_count_18m_since_disbursed, app_data_amt_count_24m_since_disbursed, app_data_amt_count_ever]  # app_data_amt_count_36m_since_disbursed, app_data_amt_count_48m_since_disbursed,app_data_amt_count_ever,app_data_amt_count_3m,app_data_amt_count_6m,app_data_amt_count_12m,
        app_data_amt_count = reduce(lambda left, right: pd.merge(left, right, on=['appid', 'accounttype'], how='outer'), dfs)

        app_data_amt_count['disb_cnt_3m_to_6m'] = app_data_amt_count.apply(lambda x: np.nan if x['disb_cnt_6m'] == 0 else float(x['disb_cnt_3m']) / x['disb_cnt_6m'], axis=1)
        app_data_amt_count['disb_cnt_6m_to_12m'] = app_data_amt_count.apply(lambda x: np.nan if x['disb_cnt_12m'] == 0 else float(x['disb_cnt_6m']) / x['disb_cnt_12m'], axis=1)
        app_data_amt_count['disb_cnt_12m_to_24m'] = app_data_amt_count.apply(lambda x: np.nan if x['disb_cnt_24m'] == 0 else float(x['disb_cnt_12m']) / x['disb_cnt_24m'], axis=1)

        app_data_amt_count['disb_amt_3m_to_6m'] = app_data_amt_count.apply(lambda x: np.nan if x['disb_amt_6m'] == 0 else float(x['disb_amt_3m']) / x['disb_amt_6m'], axis=1)
        app_data_amt_count['disb_amt_6m_to_12m'] = app_data_amt_count.apply(lambda x: np.nan if x['disb_amt_12m'] == 0 else float(x['disb_amt_6m']) / x['disb_amt_12m'], axis=1)
        app_data_amt_count['disb_amt_12m_to_24m'] = app_data_amt_count.apply(lambda x: np.nan if x['disb_amt_24m'] == 0 else float(x['disb_amt_12m']) / x['disb_amt_24m'], axis=1)

        # app_data_amt_count = app_data_amt_count.fillna(0)

        # del app_data_amt_count_3m, app_data_amt_count_6m, app_data_amt_count_12m, app_data_amt_count_18m, app_data_amt_count_latest
        return app_data_amt_count

    def calc_growth_loan_amt_count(self):

        # self.app_data_amt_count_category['loan_amt_growth_18m_12m'] = self.app_data_amt_count_category['disb_amt_12m'] / (self.app_data_amt_count_category['disb_amt_18m'] - self.app_data_amt_count_category['disb_amt_12m'])
        self.app_data_amt_count_category['loan_amt_growth_12m_6m'] = self.app_data_amt_count_category['disb_amt_6m'] / (self.app_data_amt_count_category['disb_amt_12m'] - self.app_data_amt_count_category['disb_amt_6m'])
        self.app_data_amt_count_category['loan_amt_growth_6m_3m'] = self.app_data_amt_count_category['disb_amt_3m'] / (self.app_data_amt_count_category['disb_amt_6m'] - self.app_data_amt_count_category['disb_amt_3m'])
        # self.app_data_amt_count_category['loan_amt_growth_3m_0m'] = (self.app_data_amt_count_category['disb_amt_3m']) / (self.app_data_amt_count_category['disb_loan_amt_ever'] - self.app_data_amt_count_category['disb_amt_3m'])

        # self.app_data_amt_count_category['loan_count_growth_18m_12m'] = self.app_data_amt_count_category['disb_cnt_12m'] / (self.app_data_amt_count_category['disb_cnt_18m'] - self.app_data_amt_count_category['disb_cnt_12m'])
        self.app_data_amt_count_category['loan_count_growth_12m_6m'] =  self.app_data_amt_count_category['disb_cnt_6m'] / (self.app_data_amt_count_category['disb_cnt_12m'] - self.app_data_amt_count_category['disb_cnt_6m'])
        self.app_data_amt_count_category['loan_count_growth_6m_3m'] =  self.app_data_amt_count_category['disb_cnt_3m'] / (self.app_data_amt_count_category['disb_cnt_6m'] - self.app_data_amt_count_category['disb_cnt_3m'])
        return


    def calc_writtenoff_cnt(self, cibil_df):
        app_data_wof_count = cibil_df[cibil_df['writtenoffandSettledStatus'].isin(writtenof_status)].groupby(['appid','accounttype']).agg({'writtenoffandSettledStatus': func.count_nonzero_nonnan, 'writtenoff_amt_calc': np.sum}).reset_index()
        app_data_wof_count.columns = ['appid','accounttype','writtenoff_cnt_ever', 'writtenoff_amt_ever']

        # app_data_wof_amt = cibil_df[cibil_df['writtenoffandSettledStatus'].isin(writtenof_status)].groupby(['appid']).agg({'writtenoff_amt_calc': np.sum}).reset_index()
        # app_data_wof_amt.columns = ['appid', 'writtenoff_amt_ever']

        app_data_wof_count_3m = cibil_df[((cibil_df['time_since_reported'] <= 90) & (cibil_df['writtenoffandSettledStatus'].isin(writtenof_status)))].groupby(['appid','accounttype']).agg({'writtenoffandSettledStatus': func.count_nonzero_nonnan, 'writtenoff_amt_calc': np.sum}).reset_index()
        app_data_wof_count_3m.columns = ['appid','accounttype', 'writtenoff_cnt_3m', 'writtenoff_amt_3m']

        # app_data_wof_amt_3m = cibil_df[((cibil_df['time_since_reported'] <= 90) & (cibil_df['writtenoffandSettledStatus'].isin(writtenof_status)))].groupby(['appid']).agg({'writtenoff_amt_calc': np.sum}).reset_index()
        # app_data_wof_amt_3m.columns = ['appid', 'writtenoff_amt_3m']

        app_data_wof_count_6m = cibil_df[((cibil_df['time_since_reported'] <= 180) & (cibil_df['writtenoffandSettledStatus'].isin(writtenof_status)))].groupby(['appid','accounttype']).agg({'writtenoffandSettledStatus': func.count_nonzero_nonnan, 'writtenoff_amt_calc': np.sum}).reset_index()
        app_data_wof_count_6m.columns = ['appid','accounttype', 'writtenoff_cnt_6m', 'writtenoff_amt_6m']
        # app_data_wof_count_6m = pd.DataFrame(app_data_wof_count_6m, columns=['appid', 'writtenoff_cnt_6m'])

        # app_data_wof_amt_6m = cibil_df[((cibil_df['time_since_reported'] <= 180) & (cibil_df['writtenoffandSettledStatus'].isin(writtenof_status)))].groupby(['appid']).agg({'writtenoff_amt_calc': np.sum}).reset_index()
        # app_data_wof_amt_6m.columns = ['appid', 'writtenoff_amt_6m']

        app_data_wof_count_9m = cibil_df[((cibil_df['time_since_reported'] <= 270) & (cibil_df['writtenoffandSettledStatus'].isin(writtenof_status)))].groupby(['appid','accounttype']).agg({'writtenoffandSettledStatus': func.count_nonzero_nonnan, 'writtenoff_amt_calc': np.sum}).reset_index()
        # app_data_wof_count_9m = pd.DataFrame(app_data_wof_count_9m, columns=['appid', 'writtenoff_cnt_9m'])
        app_data_wof_count_9m.columns = ['appid','accounttype', 'writtenoff_cnt_9m', 'writtenoff_amt_9m']

        # app_data_wof_amt_9m = cibil_df[((cibil_df['time_since_reported'] <= 270) & (cibil_df['writtenoffandSettledStatus'].isin(writtenof_status)))].groupby(['appid']).agg({'writtenoff_amt_calc': np.sum}).reset_index()
        # app_data_wof_amt_9m.columns = ['appid', 'writtenoff_amt_9m']

        app_data_wof_count_12m = cibil_df[((cibil_df['time_since_reported'] <= 365) & (cibil_df['writtenoffandSettledStatus'].isin(writtenof_status)))].groupby(['appid','accounttype']).agg({'writtenoffandSettledStatus': func.count_nonzero_nonnan, 'writtenoff_amt_calc': np.sum}).reset_index()
        # app_data_wof_count_12m = pd.DataFrame(app_data_wof_count_12m, columns=['appid', 'writtenoff_cnt_12m'])
        app_data_wof_count_12m.columns = ['appid','accounttype', 'writtenoff_cnt_12m', 'writtenoff_amt_12m']

        # app_data_wof_amt_12m = cibil_df[((cibil_df['time_since_reported'] <= 365) & (cibil_df['writtenoffandSettledStatus'].isin(writtenof_status)))].groupby(['appid']).agg({'writtenoff_amt_calc': np.sum}).reset_index()
        # app_data_wof_amt_12m.columns = ['appid', 'writtenoff_amt_12m']

        app_data_wof_count_24m = cibil_df[((cibil_df['time_since_reported'] <= 730) & (cibil_df['writtenoffandSettledStatus'].isin(writtenof_status)))].groupby(['appid','accounttype']).agg({'writtenoffandSettledStatus': func.count_nonzero_nonnan, 'writtenoff_amt_calc': np.sum}).reset_index()
        # app_data_wof_count_24m = pd.DataFrame(app_data_wof_count_24m, columns=['appid', 'writtenoff_cnt_24m'])
        app_data_wof_count_24m.columns = ['appid','accounttype', 'writtenoff_cnt_24m', 'writtenoff_amt_24m']
        #
        # app_data_wof_amt_24m = cibil_df[((cibil_df['time_since_reported'] <= 730) & (cibil_df['writtenoffandSettledStatus'].isin(writtenof_status)))].groupby(['appid']).agg({'writtenoff_amt_calc': np.sum}).reset_index()
        # app_data_wof_amt_24m.columns = ['appid', 'writtenoff_amt_24m']

        # app_data_wof_count_36m = cibil_df[cibil_df['time_since_reported'] <= 1080].groupby(['appid']).agg({'writtenoffandSettledStatus': func.count_nonzero_nonnan}).reset_index()
        # app_data_wof_count_36m = pd.DataFrame(app_data_wof_count_36m, columns=['appid', 'writtenoff_cnt_36m'])

        # app_data_wof_amt_36m = cibil_df[((cibil_df['time_since_reported'] <= 1080) & (~pd.isnull(cibil_df['writtenoffandSettledStatus'])))].groupby(['appid']).agg({'writtenoff_amt_calc': np.sum}).reset_index()
        # app_data_wof_amt_36m.columns = ['appid', 'writtenoff_amt_36m']
        #
        # app_data_wof_count_48m = cibil_df[cibil_df['time_since_reported'] <= 1440].groupby(['appid']).agg({'writtenoffandSettledStatus': func.count_nonzero_nonnan}).reset_index()
        # app_data_wof_count_48m = pd.DataFrame(app_data_wof_count_48m, columns=['appid', 'writtenoff_cnt_48m'])
        #
        # app_data_wof_amt_48m = cibil_df[((cibil_df['time_since_reported'] <= 1440) & (~pd.isnull(cibil_df['writtenoffandSettledStatus'])))].groupby(['appid']).agg({'writtenoff_amt_calc': np.sum}).reset_index()
        # app_data_wof_amt_48m.columns = ['appid', 'writtenoff_amt_48m']

        dfs = [app_data_wof_count, app_data_wof_count_3m, app_data_wof_count_6m,
               app_data_wof_count_12m, app_data_wof_count_9m,
               app_data_wof_count_24m]

        app_data_wof_amt_cnt = reduce(lambda left, right: pd.merge(left, right, on=['appid', 'accounttype'], how='outer'), dfs)

        app_data_wof_amt_cnt['writtenoff_amt_ever'] = app_data_wof_amt_cnt['writtenoff_amt_ever'].fillna(0)
        app_data_wof_amt_cnt['writtenoff_amt_3m'] = np.where(app_data_wof_amt_cnt['writtenoff_cnt_3m'] == 0, 0,app_data_wof_amt_cnt['writtenoff_amt_3m'])
        app_data_wof_amt_cnt['writtenoff_amt_6m'] = np.where(app_data_wof_amt_cnt['writtenoff_cnt_6m'] == 0, 0,app_data_wof_amt_cnt['writtenoff_amt_6m'])
        app_data_wof_amt_cnt['writtenoff_amt_9m'] = np.where(app_data_wof_amt_cnt['writtenoff_cnt_9m'] == 0, 0,app_data_wof_amt_cnt['writtenoff_amt_9m'])
        app_data_wof_amt_cnt['writtenoff_amt_12m'] = np.where(app_data_wof_amt_cnt['writtenoff_cnt_12m'] == 0, 0,app_data_wof_amt_cnt['writtenoff_amt_12m'])
        app_data_wof_amt_cnt['writtenoff_amt_24m'] = np.where(app_data_wof_amt_cnt['writtenoff_cnt_24m'] == 0, 0,app_data_wof_amt_cnt['writtenoff_amt_24m'])
        # app_data_wof_amt_cnt['writtenoff_amt_36m'] = np.where(app_data_wof_amt_cnt['writtenoff_cnt_36m'] == 0, 0,
        #                                                       app_data_wof_amt_cnt['writtenoff_amt_36m'])
        # app_data_wof_amt_cnt['writtenoff_amt_48m'] = np.where(app_data_wof_amt_cnt['writtenoff_cnt_48m'] == 0, 0,
        #                                                       app_data_wof_amt_cnt['writtenoff_amt_48m'])

        # app_data_wof_amt_cnt = app_data_wof_amt_cnt.fillna(0)

        return app_data_wof_amt_cnt


    def calc_max_dpd(self,cibil_df):

        max_dpd_ever = cibil_df.groupby(['appid','accounttype']).agg({'max_dpd': np.max}).reset_index()
        max_dpd_ever.columns = ['appid', 'accounttype', 'max_dpd_ever']

        max_dpd_3m = cibil_df.groupby(['appid', 'accounttype']).agg({'max_dpd_3m': np.max}).reset_index()
        max_dpd_3m.columns = ['appid', 'accounttype', 'max_dpd_3m']

        max_dpd_6m = cibil_df.groupby(['appid', 'accounttype']).agg({'max_dpd_6m': np.max}).reset_index()
        max_dpd_6m.columns = ['appid', 'accounttype', 'max_dpd_6m']

        max_dpd_9m = cibil_df.groupby(['appid', 'accounttype']).agg({'max_dpd_9m': np.max}).reset_index()
        max_dpd_9m.columns = ['appid', 'accounttype', 'max_dpd_9m']

        max_dpd_12m = cibil_df.groupby(['appid', 'accounttype']).agg({'max_dpd_12m': np.max}).reset_index()
        max_dpd_12m.columns = ['appid', 'accounttype', 'max_dpd_12m']

        max_dpd_24m = cibil_df.groupby(['appid', 'accounttype']).agg({'max_dpd_24m': np.max}).reset_index()
        max_dpd_24m.columns = ['appid', 'accounttype', 'max_dpd_24m']

        # max_dpd_36m = cibil_df.groupby(['appid', 'accounttype']).agg({'max_dpd_36m': np.max}).reset_index()
        # max_dpd_36m.columns = ['appid', 'accounttype', 'max_dpd_36m']
        #
        # max_dpd_48m = cibil_df.groupby(['appid', 'accounttype']).agg({'max_dpd_48m': np.max}).reset_index()
        # max_dpd_48m.columns = ['appid', 'accounttype', 'max_dpd_48m']


        cibil_df_close = cibil_df[~pd.isnull(cibil_df['new_dateClosed'])]
        cibil_df_open = cibil_df[pd.isnull(cibil_df['new_dateClosed'])]
        #########-------- For Closed Loans ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        max_dpd_close_ever = cibil_df_close.groupby(['appid','accounttype']).agg({'max_dpd': np.max}).reset_index()
        max_dpd_close_ever.columns = ['appid','accounttype','max_dpd_close_ever']

        max_dpd_close_3m = cibil_df_close.groupby(['appid', 'accounttype']).agg({'max_dpd_3m': np.max}).reset_index()
        max_dpd_close_3m.columns = ['appid', 'accounttype', 'max_dpd_close_3m']

        max_dpd_close_6m = cibil_df_close.groupby(['appid', 'accounttype']).agg({'max_dpd_6m': np.max}).reset_index()
        max_dpd_close_6m.columns = ['appid', 'accounttype', 'max_dpd_close_6m']

        max_dpd_close_9m = cibil_df_close.groupby(['appid', 'accounttype']).agg({'max_dpd_9m': np.max}).reset_index()
        max_dpd_close_9m.columns = ['appid', 'accounttype', 'max_dpd_close_9m']

        max_dpd_close_12m = cibil_df_close.groupby(['appid', 'accounttype']).agg({'max_dpd_12m': np.max}).reset_index()
        max_dpd_close_12m.columns = ['appid', 'accounttype', 'max_dpd_close_12m']

        max_dpd_close_24m = cibil_df_close.groupby(['appid', 'accounttype']).agg({'max_dpd_24m': np.max}).reset_index()
        max_dpd_close_24m.columns = ['appid', 'accounttype', 'max_dpd_close_24m']

        # max_dpd_close_36m = cibil_df_close.groupby(['appid', 'accounttype']).agg({'max_dpd_36m': np.max}).reset_index()
        # max_dpd_close_36m.columns = ['appid', 'accounttype', 'max_dpd_close_36m']
        #
        # max_dpd_close_48m = cibil_df_close.groupby(['appid', 'accounttype']).agg({'max_dpd_48m': np.max}).reset_index()
        # max_dpd_close_48m.columns = ['appid', 'accounttype', 'max_dpd_close_48m']

        #########-------- For Open Loans ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        max_dpd_open_ever = cibil_df_open.groupby(['appid', 'accounttype']).agg({'max_dpd': np.max}).reset_index()
        max_dpd_open_ever.columns = ['appid', 'accounttype', 'max_dpd_open_ever']

        max_dpd_open_3m = cibil_df_open.groupby(['appid', 'accounttype']).agg({'max_dpd_3m': np.max}).reset_index()
        max_dpd_open_3m.columns = ['appid', 'accounttype', 'max_dpd_open_3m']

        max_dpd_open_6m = cibil_df_open.groupby(['appid', 'accounttype']).agg({'max_dpd_6m': np.max}).reset_index()
        max_dpd_open_6m.columns = ['appid', 'accounttype', 'max_dpd_open_6m']

        max_dpd_open_9m = cibil_df_open.groupby(['appid', 'accounttype']).agg({'max_dpd_9m': np.max}).reset_index()
        max_dpd_open_9m.columns = ['appid', 'accounttype', 'max_dpd_open_9m']

        max_dpd_open_12m = cibil_df_open.groupby(['appid', 'accounttype']).agg({'max_dpd_12m': np.max}).reset_index()
        max_dpd_open_12m.columns = ['appid', 'accounttype', 'max_dpd_open_12m']

        max_dpd_open_24m = cibil_df_open.groupby(['appid', 'accounttype']).agg({'max_dpd_24m': np.max}).reset_index()
        max_dpd_open_24m.columns = ['appid', 'accounttype', 'max_dpd_open_24m']

        # max_dpd_open_36m = cibil_df_open.groupby(['appid', 'accounttype']).agg({'max_dpd_36m': np.max}).reset_index()
        # max_dpd_open_36m.columns = ['appid', 'accounttype', 'max_dpd_open_36m']
        #
        # max_dpd_open_48m = cibil_df_open.groupby(['appid', 'accounttype']).agg({'max_dpd_48m': np.max}).reset_index()
        # max_dpd_open_48m.columns = ['appid', 'accounttype', 'max_dpd_open_48m']




        dfs = [max_dpd_ever,max_dpd_3m,max_dpd_6m,max_dpd_9m,max_dpd_12m,max_dpd_24m,max_dpd_close_ever, max_dpd_close_3m,
               max_dpd_close_6m, max_dpd_close_9m,max_dpd_close_12m,max_dpd_close_24m, max_dpd_open_ever, max_dpd_open_3m,
               max_dpd_open_6m,max_dpd_open_9m, max_dpd_open_12m,max_dpd_open_24m]
        max_dpd_open_close = reduce(lambda left, right: pd.merge(left, right, on=['appid', 'accounttype'], how='outer'), dfs)

        max_dpd_open_close['max_dpd_3m_to_6m'] = max_dpd_open_close.apply(lambda x: np.nan if x['max_dpd_6m']==0 else float(x['max_dpd_3m'])/x['max_dpd_6m'],axis=1)
        max_dpd_open_close['max_dpd_6m_to_12m'] = max_dpd_open_close.apply(lambda x: np.nan if x['max_dpd_12m']==0 else float(x['max_dpd_6m'])/x['max_dpd_12m'],axis=1)
        max_dpd_open_close['max_dpd_12m_to_24m'] = max_dpd_open_close.apply(lambda x: np.nan if x['max_dpd_24m']==0 else float(x['max_dpd_12m'])/x['max_dpd_24m'],axis=1)

        return max_dpd_open_close

    def new_total_last_delinq(self,cibil_df):
        cibil_df['tag'] = cibil_df[var_list].fillna(0).ge(7.0).T.idxmax() #"""To get first column name of paymentstirngs where it goes above 7 DPD, if its not there then it will return M00'
        cibil_df['tag'] = cibil_df.apply(lambda x: np.nan if x[x['tag']] == 0 else x['tag'],axis=1)
        cibil_df['new_tag'] = np.where(pd.isnull(cibil_df['tag']),np.nan,cibil_df['tag'].astype(str)+"_phd")
        cibil_df['new_tag_val'] = cibil_df.apply(lambda x: np.nan if pd.isnull(x['new_tag']) else x[x['new_tag']],axis=1)
        agg_df = cibil_df.groupby(['appid','accounttype'])['new_tag_val'].apply(list).reset_index(name='new')
        agg_df['new'] = agg_df.apply(lambda x: [i for i in x['new'] if i == i],axis=1)
        agg_df['min_phd_date'] = agg_df['new'].apply(lambda x: np.nan if len(x)==0 else max(x))
        agg_df = pd.merge(agg_df,cibil_df[['appid','accounttype','CREATED_TIME']].drop_duplicates(),on=['appid','accounttype'],how='left')
        agg_df['month_since_last_delinquency'] = agg_df.apply(lambda x: int((x['CREATED_TIME'] - x['min_phd_date']).days / 30) if (not pd.isnull(x['min_phd_date'])) else np.nan, axis=1)
        # agg_df['month_since_last_delinquency'] = np.where(agg_df['min_phd_date']=='',np.nan,(agg_df['CREATED_TIME'] - agg_df['min_phd_date']).dt.days / 30.0)

        return agg_df[['appid','accounttype','month_since_last_delinquency']]

    def calc_delay_emi_cnts(self,cibil_df):
        emi_cnt_perc = cibil_df[['appid','accounttype']].drop_duplicates()
        for delay in delinquencies:
            for mo in ['ever','3m','6m','9m','12m','24m']:
                cnt_col = 'emi_count_'+ str(mo) +"_" + str(delay)
                perc_col = 'emi_count_'+ str(mo) +"_" + str(delay)+ "plus_perc"
                my_df = cibil_df.groupby(['appid', 'accounttype']).agg({perc_col:np.mean,cnt_col:np.sum}).reset_index()
                emi_cnt_perc = pd.merge(emi_cnt_perc,my_df,on=['appid','accounttype'],how='outer')

        acct_agg_perc = self.calc_delay_loan_cnts(cibil_df)

        print("")

        final_perc_calc = pd.merge(emi_cnt_perc,acct_agg_perc,on=['appid','accounttype'],how='outer')

        for delay in delinquencies:
            for mo in ['ever','3m','6m','9m','12m','24m']:
                num_col = 'emi_count_'+ str(mo) +"_" + str(delay)
                # den_col = 'accts_'+str(delay)+"plus_"+str(mo)
                if mo != 'ever':
                    den_col = 'acct_for_' + str(mo)
                else:
                    den_col = 'total_accts'
                final_perc_calc['emi_count_per_acct_'+str(mo)+"_"+str(delay)+"plus_perc"] = final_perc_calc.apply(lambda x: np.nan if x[den_col]==0 else float(x[num_col])/x[den_col],axis=1)

        return final_perc_calc

    def calc_delay_loan_cnts(self,cibil_df):
        agg_act_cnt = cibil_df[['appid', 'accounttype']].drop_duplicates()
        agg_acct_delay_cnt = cibil_df[['appid', 'accounttype']].drop_duplicates()

        """ Denominator """
        for mo in ['ever','1m','3m','6m','9m','12m','24m']:
            if mo != 'ever':
                col = 'acct_for_' + str(mo)
            else:
                col = 'total_accts'
            my_df = cibil_df.groupby(['appid', 'accounttype']).agg({col:np.sum}).reset_index()
            agg_act_cnt = pd.merge(agg_act_cnt,my_df,on=['appid','accounttype'],how='outer')

        for delay in delinquencies:
            for mo in ['ever','1m','3m','6m','9m','12m','24m']:
                col = 'accts_' + str(delay)+ "plus_"+ str(mo)
                my_df = cibil_df.groupby(['appid', 'accounttype']).agg({col:np.sum}).reset_index()
                agg_acct_delay_cnt = pd.merge(agg_acct_delay_cnt,my_df,on=['appid','accounttype'],how='outer')

        final_agg = pd.merge(agg_act_cnt,agg_acct_delay_cnt,on=['appid','accounttype'],how='inner')

        for delay in delinquencies:
            for mo in ['ever','1m','3m','6m','9m','12m','24m']:
                num_col = 'accts_' + str(delay)+ "plus_"+ str(mo)

                if mo != 'ever':
                    den_col = 'acct_for_' + str(mo)
                else:
                    den_col = 'total_accts'

                final_agg[num_col+"_perc"] = final_agg.apply(lambda x: np.nan if x[den_col]==0 else float(x[num_col])/x[den_col],axis=1)


        # final_agg = pd.merge(final_agg, self.outstanding_agg, on=['appid', 'accounttype'], how='inner')

        return final_agg

    def current_outstanding_balance(self,cibil_df):

        outstanding_bal = cibil_df.groupby(['appid','accounttype']).agg({'current_os_bal':np.sum,'current_os_bal_30plus':np.sum,'current_os_bal_60plus':np.sum,'current_os_bal_90plus':np.sum}).reset_index()

        return outstanding_bal

    def youngest_loan(self, cibil_df):
        youngest_loan = cibil_df.groupby(['appid', 'accounttype']).agg({'dateopened': np.max}).reset_index()
        youngest_loan = pd.merge(youngest_loan, cibil_df[['appid', 'created_time']].drop_duplicates(), on='appid', how='left')
        youngest_loan['youngest_loan'] = round((youngest_loan['created_time'] - youngest_loan['dateopened']).dt.days/30, 0)
        return youngest_loan[['appid', 'accounttype', 'youngest_loan']]

    def oldest_loan(self, cibil_df):
        oldest_loan = cibil_df.groupby(['appid', 'accounttype']).agg({'dateopened': np.min}).reset_index()
        oldest_loan = pd.merge(oldest_loan, cibil_df[['appid', 'created_time']].drop_duplicates(), on='appid', how='left')
        oldest_loan['oldest_loan'] = round((oldest_loan['created_time'] - oldest_loan['dateopened']).dt.days / 30, 0)
        return oldest_loan[['appid', 'accounttype', 'oldest_loan']]

    def delay_amount(self, cibil_df):
        filtered_df = cibil_df.query("M00 >=7 and dateClosed == 'NaT'")
        delayed_df = filtered_df.groupby(['appid', 'accounttype']).agg({'current_os_bal': np.sum, 'appid': [func.count_nonzero_nonnan]}).reset_index()
        delayed_df.columns = ['appid', 'accounttype', 'delay_amount', 'count_of_delay_loans']
        return delayed_df

    def pivot_processing_loan(self,df):

        cnt = 0
        for col in df.columns.values:
            if (col != 'appid') and (col != 'accounttype'):
                pivot = func.pivot_cibil(data=df, col='accounttype', val=col, suffix='_' + str(col),nan_zero_flag=False)
                if cnt == 0:
                    master_pivot = pivot.copy()
                else:
                    master_pivot = pd.merge(master_pivot, pivot, how='left', on='appid')
                cnt += 1

        return master_pivot