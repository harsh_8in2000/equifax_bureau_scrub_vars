import numpy as np
import pandas as pd


def acct_to_enq(final_df):

    keymap = {'account_enq_12m': ['disb_cnt_12m', 'enq_cnt_12m'],
              'Consumer Loan_account_enq_6m': ['Consumer Loan_disb_cnt_6m', 'Consumer Loan_enq_cnt_6m'],
              'Consumer Loan_account_enq_12m': ['Consumer Loan_disb_cnt_12m', 'Consumer Loan_enq_cnt_12m'],
              'cc_account_enq_12m': ['cc_disb_cnt_12m', 'cc_enq_cnt_12m']}
    for key, value in keymap.items():
        loan_key = value[0]
        enq_key = value[1]
        var_key = key
        for var in value:
            if var not in final_df.columns:
                final_df[var] = np.nan
        final_df[var_key] = final_df.apply(lambda x: x[loan_key] / float(x[enq_key]) if (x[loan_key] >= 0 and x[enq_key] > 0) else 0 if (pd.isnull(x[loan_key]) and x[enq_key] > 0) else 1 if (x[loan_key] > 0 and (x[enq_key] == 0 or pd.isnull(x[enq_key]))) else np.nan, axis=1)

    return final_df
