import pandas as pd
from functools import reduce
from utils import util_func as func

class ENQUIRY_AGGREGATION():
    def __init__(self, dc, accountype_map):
        enq_df_category = pd.merge(dc._enqSegCl, accountype_map, right_on='accounttype',left_on='product_type', how='left')
        coll_acc_list = enq_df_category['collateralized'].dropna().unique().tolist()
        del enq_df_category['ticket size'], enq_df_category['collateralized']
        enq_df_category['collateralized'] = enq_df_category['product_type'].apply(lambda x: 'collateralized' if str(x).lower() in coll_acc_list else 'non-collateralized')
        del enq_df_category['product_type']

        self.total_enq_calc = self.total_calc_enq_amt_count(dc._enqSegCl)
        self.enq_cnt = self.calc_enq_amt_count(enq_df_category)
        self.enq_cnt_category = self.pivot_processing_enq(self.enq_cnt)

        df2s = []
        var_list = ['business_consumer_cc']
        for var in var_list:
            del enq_df_category['accounttype']
            enq_df_category.rename(columns={var: 'accounttype'}, inplace=True)
            self.enq_cnt_sub_category = self.calc_enq_amt_count(enq_df_category)
            df2s.append(self.enq_cnt_sub_category)

        self.sub_category_enq_calc = reduce(lambda left, right: pd.merge(left, right, on=['appid', 'accounttype'], how='outer'), df2s)
        self.enq_cnt_sub_category = self.pivot_processing_enq(self.sub_category_enq_calc)

        self.final_enq = pd.merge(self.total_enq_calc, self.enq_cnt_category, on='appid', how='left')
        self.final_enq = pd.merge(self.final_enq, self.enq_cnt_sub_category, on='appid', how='left')
        pass

    def calc_enq_amt_count(self, cibil_enq):
        enq_data_amt_count = cibil_enq.groupby(['appid','accounttype']).agg({'enq_amt': [func.count_nonzero_nonnan]}).reset_index()
        enq_data_amt_count.columns = ['appid','accounttype', 'enq_cnt']

        enq_data_amt_count_3m = cibil_enq[cibil_enq['age_of_loan'] <= 90].groupby(['appid','accounttype']).agg({'enq_amt': [func.count_nonzero_nonnan]}).reset_index()
        enq_data_amt_count_3m.columns = ['appid','accounttype', 'enq_cnt_3m']

        enq_data_amt_count_6m = cibil_enq[cibil_enq['age_of_loan'] <= 180].groupby(['appid','accounttype']).agg({'enq_amt': [func.count_nonzero_nonnan]}).reset_index()
        enq_data_amt_count_6m.columns = ['appid','accounttype', 'enq_cnt_6m']

        enq_data_amt_count_9m = cibil_enq[cibil_enq['age_of_loan'] <= 270].groupby(['appid','accounttype']).agg({'enq_amt': [func.count_nonzero_nonnan]}).reset_index()
        enq_data_amt_count_9m.columns = ['appid','accounttype', 'enq_cnt_9m']

        enq_data_amt_count_12m = cibil_enq[cibil_enq['age_of_loan'] <= 365].groupby(['appid','accounttype']).agg({'enq_amt': [func.count_nonzero_nonnan]}).reset_index()
        enq_data_amt_count_12m.columns = ['appid','accounttype', 'enq_cnt_12m']

        enq_data_amt_count_24m = cibil_enq[cibil_enq['age_of_loan'] <= 730].groupby(['appid','accounttype']).agg({'enq_amt': [func.count_nonzero_nonnan]}).reset_index()
        enq_data_amt_count_24m.columns = ['appid','accounttype', 'enq_cnt_24m']

        # enq_data_amt_count_36m = cibil_enq[cibil_enq['age_of_loan'] <= 1080].groupby(['appid','accounttype']).agg({'enq_amt': [func.count_nonzero_nonnan]}).reset_index()
        # enq_data_amt_count_36m.columns = ['appid','accounttype', 'enq_cnt_36m']
        #
        # enq_data_amt_count_48m = cibil_enq[cibil_enq['age_of_loan'] <= 1440].groupby(['appid','accounttype']).agg({'enq_amt': [func.count_nonzero_nonnan]}).reset_index()
        # enq_data_amt_count_48m.columns = ['appid','accounttype', 'enq_cnt_48m']

        enq_data_amt_count = pd.merge(enq_data_amt_count, enq_data_amt_count_3m, on=['appid','accounttype'], how='left')
        enq_data_amt_count = pd.merge(enq_data_amt_count, enq_data_amt_count_6m, on=['appid','accounttype'], how='left')
        enq_data_amt_count = pd.merge(enq_data_amt_count, enq_data_amt_count_9m, on=['appid','accounttype'], how='left')
        enq_data_amt_count = pd.merge(enq_data_amt_count, enq_data_amt_count_12m, on=['appid','accounttype'], how='left')
        enq_data_amt_count = pd.merge(enq_data_amt_count, enq_data_amt_count_24m, on=['appid','accounttype'], how='left')
        # enq_data_amt_count = pd.merge(enq_data_amt_count, enq_data_amt_count_36m, on=['appid','accounttype'], how='left')
        # enq_data_amt_count = pd.merge(enq_data_amt_count, enq_data_amt_count_48m, on=['appid','accounttype'], how='left')


        return enq_data_amt_count

    def total_calc_enq_amt_count(self, cibil_enq):
        enq_data_amt_count = cibil_enq.groupby(['appid']).agg({'enq_amt': [func.count_nonzero_nonnan]}).reset_index()
        enq_data_amt_count.columns = ['appid', 'enq_cnt']

        enq_data_amt_count_3m = cibil_enq[cibil_enq['age_of_loan'] <= 90].groupby(['appid']).agg({'enq_amt': [func.count_nonzero_nonnan]}).reset_index()
        enq_data_amt_count_3m.columns = ['appid','enq_cnt_3m']

        enq_data_amt_count_6m = cibil_enq[cibil_enq['age_of_loan'] <= 180].groupby(['appid']).agg({'enq_amt': [func.count_nonzero_nonnan]}).reset_index()
        enq_data_amt_count_6m.columns = ['appid', 'enq_cnt_6m']

        enq_data_amt_count_9m = cibil_enq[cibil_enq['age_of_loan'] <= 270].groupby(['appid']).agg({'enq_amt': [func.count_nonzero_nonnan]}).reset_index()
        enq_data_amt_count_9m.columns = ['appid', 'enq_cnt_9m']

        enq_data_amt_count_12m = cibil_enq[cibil_enq['age_of_loan'] <= 365].groupby(['appid']).agg({'enq_amt': [func.count_nonzero_nonnan]}).reset_index()
        enq_data_amt_count_12m.columns = ['appid', 'enq_cnt_12m']

        enq_data_amt_count_24m = cibil_enq[cibil_enq['age_of_loan'] <= 730].groupby(['appid']).agg({'enq_amt': [func.count_nonzero_nonnan]}).reset_index()
        enq_data_amt_count_24m.columns = ['appid', 'enq_cnt_24m']

        # enq_data_amt_count_36m = cibil_enq[cibil_enq['age_of_loan'] <= 1080].groupby(['appid']).agg({'enq_amt': [func.count_nonzero_nonnan]}).reset_index()
        # enq_data_amt_count_36m.columns = ['appid', 'enq_cnt_36m']
        #
        # enq_data_amt_count_48m = cibil_enq[cibil_enq['age_of_loan'] <= 1440].groupby(['appid']).agg({'enq_amt': [func.count_nonzero_nonnan]}).reset_index()
        # enq_data_amt_count_48m.columns = ['appid','enq_cnt_48m']

        enq_data_amt_count = pd.merge(enq_data_amt_count, enq_data_amt_count_3m, on=['appid'], how='left')
        enq_data_amt_count = pd.merge(enq_data_amt_count, enq_data_amt_count_6m, on=['appid'], how='left')
        enq_data_amt_count = pd.merge(enq_data_amt_count, enq_data_amt_count_9m, on=['appid'], how='left')
        enq_data_amt_count = pd.merge(enq_data_amt_count, enq_data_amt_count_12m, on=['appid'], how='left')
        enq_data_amt_count = pd.merge(enq_data_amt_count, enq_data_amt_count_24m, on=['appid'], how='left')
        # enq_data_amt_count = pd.merge(enq_data_amt_count, enq_data_amt_count_36m, on=['appid'], how='left')
        # enq_data_amt_count = pd.merge(enq_data_amt_count, enq_data_amt_count_48m, on=['appid'], how='left')


        return enq_data_amt_count

    def pivot_processing_enq(self,df):

        cnt = 0
        for col in df.columns.values:
            if (col != 'appid') and (col != 'accounttype'):
                pivot = func.pivot_cibil(data=df, col='accounttype', val=col, suffix='_' + str(col),nan_zero_flag=True)
                if cnt == 0:
                    master_pivot = pivot.copy()
                else:
                    master_pivot = pd.merge(master_pivot, pivot, how='left', on='appid')
                cnt += 1

        return master_pivot