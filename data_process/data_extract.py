import pandas as pd
from configs.config import INPUT,mode,data_mode
class DATA_EXTRACT():
    def __init__(self):
        self._acctSeg = pd.DataFrame()
        self._enqSeg = pd.DataFrame()
        self._scoreSeg = pd.DataFrame()
        pass

    def get_all_segments(self,ids,scrub_dt):

        if mode == 'file':
            if data_mode == 'scrub':
                self._acctSeg = self.read_file(INPUT + 'Accretive_Retail_Retro.txt','|','csv')
                self._enqSeg = self.read_file(INPUT + 'Inquiry.txt','|','csv')

                self._acctSeg['RETRO_DATE'] = pd.to_datetime(self._acctSeg['RETRO_DATE'])
                self._enqSeg['Retro date'] = pd.to_datetime(self._enqSeg['Retro date'])

                self._acctSeg = self._acctSeg[((self._acctSeg['REFERENCE_NO'].isin(ids)) & (self._acctSeg['RETRO_DATE'] == scrub_dt))]
                self._enqSeg = self._enqSeg[((self._enqSeg['REFERENCE_NO'].isin(ids)) & (self._enqSeg['Retro date'] == scrub_dt))]

                self.weightage = self.read_file(INPUT + 'weightage.csv', ',', 'csv')
                self._acctSeg = pd.merge(self._acctSeg, self.weightage, on='ACCOUNTTYPE', how='left')

                self._scoreSeg = self._acctSeg[['REFERENCE_NO','SCORE']].drop_duplicates()
            else:
                self._acctSeg = self.read_file(INPUT)
                self._enqSeg = self.read_file(INPUT)


        elif mode == 'db':
            if data_mode == 'scrub':
                self._acctSeg = pd.read_sql()
            else:
                self._acctSeg = pd.read_sql()

        return


    def read_file(self,filename,deli,filetype='csv'):
        if filetype == 'csv':
            df = pd.read_csv(filename,delimiter=deli,encoding_errors='ignore')
        elif filetype == 'excel':
            df = pd.read_excel(filename)

        return df
