import re
import pandas as pd
import numpy as np
from configs.constant import acct_col_rename_dict,enq_col_rename_dict,var_list
from configs.config import data_mode
class DATA_CLEAN():
    def __init__(self,de):
        self._acctSegCl = de._acctSeg.copy(deep=True)
        self._enqSegCl = de._enqSeg.copy(deep=True)

        self._scoreSegCl = de._scoreSeg.copy(deep=True)
        # self._phoneSegCl = de._phoneSeg.copy(deep=True)
        # self._empSegCl = de._empSeg.copy(deep=True)
        # self._addressSegCl = de._addressSeg.copy(deep=True)
        self.cleaning()
        pass

    def cleaning(self):
        collist = list(acct_col_rename_dict.keys())
        act_collist = list(self._acctSegCl.columns)
        collist = [x for x in collist if x not in act_collist] + var_list
        for col in collist:
            self._acctSegCl[col] = np.nan

        self._acctSegCl.rename(columns=acct_col_rename_dict, inplace=True)
        self._enqSegCl.rename(columns=enq_col_rename_dict, inplace=True)

        self._acctSegCl['dateclosed'] = np.where(self._acctSegCl['dateclosed'] == '1900-01-01', np.nan,
                                                 self._acctSegCl['dateclosed'])
        self._acctSegCl['accounttype'] = self._acctSegCl['accounttype'].replace("–", "-", regex=True)
        self._enqSegCl['product_type'] = self._enqSegCl['product_type'].replace("–", "-", regex=True)

        self.acctseg_writtenof_suitfiled_status_standardization()
        self.acctseg_date_standardisation()
        self.acctseg_amt_standardisation()
        self.acctseg_accttype_standardisation()
        self.acctseg_filtering()
        self.acctseg_level_calc()
        self.clean_cibil_enq()

        return

    def acctseg_date_standardisation(self):
        cols = ['dateopened', 'datereported', 'created_time', 'lastpaymentdate','dateclosed']
        # self._acctSegCl[cols] = self._acctSegCl[cols].apply(pd.to_datetime(dayfirst=True), errors='coerce')
        for col in cols:
            self._acctSegCl[col] = pd.to_datetime(self._acctSegCl[col], dayfirst=True, errors='coerce')
        return

    def acctseg_writtenof_suitfiled_status_standardization(self):
        if data_mode == 'scrub':
            self._acctSegCl['suitfiledstatus'] = self._acctSegCl['suitfiledstatus'].fillna(np.nan)
            self._acctSegCl['assetclassification'] = self._acctSegCl['assetclassification'].fillna(np.nan)
            self._acctSegCl['accountstatus'] = self._acctSegCl['accountstatus'].fillna(np.nan)


        return

    def acctseg_amt_standardisation(self):
        cols = ['balance','sanctionamount','collateralvalue','creditlimit','highcredit','installmentamount','lastpayment','pastdueamount'] #, 'cibil_personal_score'
        # for col in cols:
        #     if col in self._acctSegCl.columns:
        #         # self._acctSegCl[col] = self._acctSegCl[col].apply(lambda x: str(x).replace(",", '') if pd.isnull(x) == False else x)
        #         self._acctSegCl[col] = np.where(~pd.isnull(self._acctSegCl[col]),self._acctSegCl[col].replace(r'(?:\[|\]|\,|\')', '', regex=True),self._acctSegCl[col])
        #         self._acctSegCl[col] = pd.to_numeric(self._acctSegCl[col])
        self._acctSegCl[cols] = self._acctSegCl[cols].replace(r'(?:\[|\]|\,|\')', '', regex=True)
        self._acctSegCl[cols] = self._acctSegCl[cols].apply(pd.to_numeric)

        self._acctSegCl['sanctionamount'] = self._acctSegCl['sanctionamount'].fillna(0)
        self._acctSegCl['balance'] = self._acctSegCl['balance'].fillna(0)
        self._acctSegCl['creditlimit'] = self._acctSegCl['creditlimit'].fillna(0)
        self._acctSegCl['highcredit'] = self._acctSegCl['highcredit'].fillna(0)

        self._acctSegCl['sanctionamount'] = np.where(~pd.isnull(self._acctSegCl['sanctionamount']), self._acctSegCl['sanctionamount'],
                                                                np.where(~pd.isnull(self._acctSegCl['creditlimit']), self._acctSegCl['creditlimit'],
                                                                         np.where(~pd.isnull(self._acctSegCl['balance']), self._acctSegCl['balance'],
                                                                                  np.where((~pd.isnull(self._acctSegCl['installmentamount'])) & (
                                                                                      ~pd.isnull(self._acctSegCl['repaymenttenure'])),
                                                                                           self._acctSegCl['installmentamount'] * self._acctSegCl['repaymenttenure'],
                                                                                           np.where((self._acctSegCl['installmentamount'] >= 1000) & (
                                                                                               pd.isnull(self._acctSegCl['repaymenttenure'])),
                                                                                                    self._acctSegCl['installmentamount'], np.nan)))))

        self._acctSegCl['sanctionamount'] = self._acctSegCl.apply(lambda x: max(x['sanctionamount'],x['creditlimit'],x['highcredit']) if x['sanctionamount']==0 else x['sanctionamount'],axis=1)
        self._acctSegCl['creditlimit_for_income'] = self._acctSegCl.apply(lambda x: max(x['sanctionamount'], x['creditlimit']),axis=1)
        return

    def acctseg_accttype_standardisation(self):
        self._acctSegCl['accounttype'] = self._acctSegCl['accounttype'].replace('[^\w+\s+()-/]', '', regex=True).astype(str)
        self._acctSegCl = self._acctSegCl[~pd.isnull(self._acctSegCl['accounttype'])].reset_index(drop=True)

        self._acctSegCl['accounttype'] = self._acctSegCl['accounttype'].apply(lambda x: x if ((x.lower() in ('business loan against bank deposits','business non-funded credit facility')) or (x.find('Business Loan') == -1)) else 'Business Loan' if x.find('Business Loan') >= 0 else x)
        self._acctSegCl['accounttype'] = self._acctSegCl['accounttype'].apply(lambda x: x if x.find('Microfinance') == -1 else 'Microfinance')
        self._acctSegCl['accounttype'] = self._acctSegCl['accounttype'].apply(lambda x: x if x.lower().find('business non-funded credit facility') == -1 else 'Business Non-Funded Credit Facility')
        self._acctSegCl['accounttype'] = self._acctSegCl['accounttype'].apply(lambda x: x if x.lower().find('business loan against bank deposits') == -1 else 'Business Loan Against Bank Deposits')

        return

    def acctseg_filtering(self):

        self._acctSegCl = self._acctSegCl[~pd.isnull(self._acctSegCl['sanctionamount'])].reset_index(drop=True)
        self._acctSegCl = self._acctSegCl.query('sanctionamount > 0.0').reset_index(drop=True)
        assert len(self._acctSegCl) >0
        # self._acctSegCl['dateopened'] = np.where(pd.isnull(self._acctSegCl['dateopened']),self._acctSegCl['paymentHistoryEndDate'],self._acctSegCl['dateopened'])
        self.filter_loan_data()
        return

    def filter_loan_data(self):
        drop_duplicate_column_list = ['appid', 'acct_number', 'accounttype', 'assetclassification', 'balance',
       'sanctionamount', 'collateraltype', 'collateralvalue', 'creditlimit',
       'dateopened', 'datereported', 'highcredit', 'installmentamount',
       'lastpayment', 'lastpaymentdate', 'ownershiptype', 'pastdueamount',
       'repaymenttenure', 'terms_frequency',
       'suitfiledstatus', 'accountstatus_history', 'assetclass_history',
       'suitfiled_history', 'acct_uniq_id', 'csmr_nbr', 'bureau_score',
       'self_trade', 'created_time', 'accountstatus', 'dateclosed', 'sector']

        self._acctSegCl = self._acctSegCl[self._acctSegCl['ownershiptype'].str.lower() != 'guarantor']
        self._acctSegCl = self._acctSegCl[self._acctSegCl['ownershiptype'].str.lower() != 'authorised user']
        joint_owner_df = self._acctSegCl[self._acctSegCl['ownershiptype'].str.lower() == 'joint'].reset_index(drop=True)
        individual_owner_df = self._acctSegCl[pd.isnull(self._acctSegCl['ownershiptype'])].reset_index(drop=True)
        joint_owner_df = joint_owner_df.drop_duplicates(subset=drop_duplicate_column_list, keep='first')
        # self._acctSegCl = individual_owner_df.append(joint_owner_df, ignore_index=True)
        self._acctSegCl = pd.concat([individual_owner_df, joint_owner_df], ignore_index=True)
        self._acctSegCl['ownershiptype'] = self._acctSegCl['ownershiptype'].apply(lambda x: 'Individual' if pd.isna(x) else x)
        self._acctSegCl.sort_values(['appid', 'created_time'], inplace=True)

        return

    def acctseg_level_calc(self):

        # self._acctSegCl['age_of_loan'] = self._acctSegCl.apply(lambda x: int((x['CREATED_TIME'] - x['dateOpened_Disbursed']).days), axis=1)
        self._acctSegCl['age_of_loan'] = (self._acctSegCl['created_time'] - self._acctSegCl['dateopened']).dt.days

        self._acctSegCl['latest'] = self._acctSegCl.groupby(['appid'])['dateopened'].rank(ascending=False)
        # self._acctSegCl['MonthDiff'] = self._acctSegCl.apply(lambda z: z['age_of_loan'] / 30.0, axis=1)
        # self._acctSegCl['MonthDiff'] = self._acctSegCl['age_of_loan'] / 30.0
        # self._acctSegCl['age_of_closure'] = self._acctSegCl.apply(lambda row: np.nan if pd.isnull(row['dateClosed']) else (row['CREATED_TIME'] - row['dateClosed']).days,axis=1)
        self._acctSegCl['age_of_closure'] = np.where(pd.isnull(self._acctSegCl['dateclosed']), np.nan, (self._acctSegCl['created_time'] - self._acctSegCl['dateclosed']).dt.days)
        # self._acctSegCl['time_since_reported'] = self._acctSegCl.apply(lambda x: int((x['created_time'] - x['new_dateReportedandCertified']).days), axis=1)
        # self._acctSegCl['new_dateReportedandCertified'] = pd.to_datetime(self._acctSegCl['datereported_trades'])
        self._acctSegCl['time_since_reported'] = (self._acctSegCl['created_time'] - self._acctSegCl['datereported']).dt.days
        self._acctSegCl['writtenoff_flag'] = np.where(self._acctSegCl['accountstatus'].isin(['Written Off','Charge Off/Written Off','Suit Filed','Settled']),1,0)
        self._acctSegCl['restructuring_flag'] = np.where(self._acctSegCl['accountstatus'].isin(['Restructured Loan', 'Restructured Due to COVID19', 'Restructured due to Natural Calamity','Restructured Loan - Govt Mandate']), 1, 0)
        #
        # self._acctSegCl['new_dateClosed'] = np.where(((self._acctSegCl['balance'] == 0) & (
        #     pd.isnull(self._acctSegCl['dateClosed'])) & (~self._acctSegCl['accounttype'].str.contains(
        #     r'Credit Card|Overdraft', regex=True))), self._acctSegCl['datereported'],
        #                                              self._acctSegCl['dateclosed'])
        # self._acctSegCl['time_since_closed'] = self._acctSegCl.apply(
        #     lambda x: np.nan if pd.isnull(x['new_dateClosed']) else int((x['CREATED_TIME'] - x['new_dateClosed']).days),
        #     axis=1)

        #
        # print("  --Cleaning Stage 2")
        self.seperate_acct_history()
        self.clean_emi_dpd()
        self.get_dpd_range_from_account_status()
        # self.create_payment_dates()
        # self._acctSegCl['new_CREATED_TIME'] = pd.to_datetime(self._acctSegCl['CREATED_TIME'].dt.date, dayfirst=True)

        return

    def seperate_acct_history(self):
        self._acctSegCl[var_list] = pd.DataFrame(self._acctSegCl['accountstatus_history'].apply(list).tolist())
        return

    def clean_emi_dpd(self):
        self._acctSegCl[var_list] = self._acctSegCl[var_list].replace('nan', np.nan)
        self._acctSegCl[var_list] = self._acctSegCl[var_list].replace(r'(?:\s+)', np.nan, regex=True)
        for var in var_list:
            self._acctSegCl[var] = np.where(self._acctSegCl[var] == '1', '1-29', self._acctSegCl[var])
            self._acctSegCl[var] = np.where(self._acctSegCl[var] == '2', '30-59', self._acctSegCl[var])
            self._acctSegCl[var] = np.where(self._acctSegCl[var] == '3', '60-89', self._acctSegCl[var])
            self._acctSegCl[var] = np.where(self._acctSegCl[var] == '4', '90-119', self._acctSegCl[var])
            self._acctSegCl[var] = np.where(self._acctSegCl[var] == '5', '120-179', self._acctSegCl[var])
            self._acctSegCl[var] = np.where(self._acctSegCl[var] == '6', '180-359', self._acctSegCl[var])
            self._acctSegCl[var] = np.where(self._acctSegCl[var] == 'B', '360-539', self._acctSegCl[var])
            self._acctSegCl[var] = np.where(self._acctSegCl[var] == 'C', '540-719', self._acctSegCl[var])
            self._acctSegCl[var] = np.where(self._acctSegCl[var] == 'F', '720+', self._acctSegCl[var])
            # self._acctSegCl[var] = self._acctSegCl[var].apply(lambda x: x if pd.isnull(x) else str(x).replace('\x00\x00\x00', np.nan))
            # try:
            #     self._acctSegCl[var] = np.where(self._acctSegCl[var].isin(['XXX', 'OFF', 'DFL', 'ICE', 'F-3', '12N', 'OOR', 'ESP', 'VTL', 'TD0', '']), 0, self._acctSegCl[var])
            #     self._acctSegCl[var] = np.where(self._acctSegCl[var] == 'STD', 0, self._acctSegCl[var])
            #     self._acctSegCl[var] = np.where(self._acctSegCl[var] == 'SMA', 60, self._acctSegCl[var])
            #     self._acctSegCl[var] = np.where(self._acctSegCl[var] == 'SUB', 90, self._acctSegCl[var])
            #     self._acctSegCl[var] = np.where(self._acctSegCl[var] == 'DBT', 180, self._acctSegCl[var])
            #     self._acctSegCl[var] = np.where(self._acctSegCl[var] == 'LSS', 180, self._acctSegCl[var])
            #     self._acctSegCl[var] = pd.to_numeric(self._acctSegCl[var])
            # except:
            #     self._acctSegCl[var] = pd.to_numeric(self._acctSegCl[var])
        return

    def clean_cibil_enq(self):
        # self._enqSegCl['purpose'] = self._enqSegCl['purpose'].apply(lambda x: clean_string(x))
        if 'product_type' in self._enqSegCl.columns:
            self._enqSegCl['product_type'] = self._enqSegCl['product_type'].replace('[^\w+\s+()-/]', '', regex=True)
            self._enqSegCl = self._enqSegCl[~pd.isnull(self._enqSegCl['product_type'])].reset_index(drop=True)
            self._enqSegCl = self._enqSegCl[self._enqSegCl['product_type'] != ""].reset_index(drop=True)
            self._enqSegCl['product_type'] = self._enqSegCl['product_type'].astype(str)
            self._enqSegCl['product_type'] = self._enqSegCl['product_type'].apply(lambda x: x if ((x.lower() in ('business loan against bank deposits', 'business non-funded credit facility')) or (x.find('Business Loan') == -1)) else 'Business Loan' if x.find('Business Loan') >= 0 else x)
            # self._enqSegCl['product_type'] = self._enqSegCl['product_type'].apply(lambda x: x if x.find('Business Loan') == -1 else 'Business Loan')
            self._enqSegCl['product_type'] = self._enqSegCl['product_type'].apply(lambda x: x if x.find('Microfinance') == -1 else 'Microfinance')
            self._enqSegCl['product_type'] = self._enqSegCl['product_type'].apply(lambda x: x if x.lower().find('business non-funded credit facility') == -1 else 'Business Non-Funded Credit Facility')
            self._enqSegCl['product_type'] = self._enqSegCl['product_type'].apply(lambda x: x if x.lower().find('business loan against bank deposits') == -1 else 'Business Loan Against Bank Deposits')

            # self._enqSegCl['purpose'] = self._enqSegCl['purpose'].apply(lambda x: x if x.find('Business Non-Funded Credit Facility') == -1 else 'Business Non-Funded Credit Facility')
            # self._enqSegCl['purpose'] = self._enqSegCl['purpose'].apply(lambda x: "Other" if ((x in other_account_types) or pd.isnull(x)) else x)

        self._enqSegCl = self._enqSegCl[~pd.isnull(self._enqSegCl['enq_amt'])].reset_index(drop=True)

        # self._enqSegCl['amount'] = self._enqSegCl['amount'].astype(str)

        self._enqSegCl['enq_amt'] = pd.to_numeric(self._enqSegCl['enq_amt']) #.str.replace(',', '')
        self._enqSegCl = self._enqSegCl[~pd.isnull(self._enqSegCl['enq_date'])]
        # self._enqSegCl['dateofEnquiry'] = self._enqSegCl['dateofEnquiry'].apply(lambda x: self.convert_date(x))
        self._enqSegCl['enq_date'] = pd.to_datetime(self._enqSegCl['enq_date'])
        self._enqSegCl['created_time'] = pd.to_datetime(self._enqSegCl['created_time'])
        self._enqSegCl = self._enqSegCl[~pd.isnull(self._enqSegCl['enq_date'])].reset_index(drop=True)

        # self._enqSegCl['CREATED_TIME'] = pd.to_datetime(self._enqSegCl['CREATED_TIME'])
        if len(self._enqSegCl) > 0:
            # self._enqSegCl['age_of_loan'] = self._enqSegCl.apply(lambda x: int((x['CREATED_TIME'] - x['dateofEnquiry']).days),axis=1)
            self._enqSegCl['age_of_loan'] = (self._enqSegCl['created_time'] - self._enqSegCl['enq_date']).dt.days
            self._enqSegCl['latest'] = self._enqSegCl.groupby(['appid'])['enq_date'].rank(ascending=False)
            # self._enqSegCl['MonthDiff'] = self._enqSegCl.apply(lambda z: z['age_of_loan'] / 30.0, axis=1)
            self._enqSegCl['MonthDiff'] = self._enqSegCl['age_of_loan'] / 30.0

        return

    def get_dpd_range_from_account_status(self):
        self._acctSegCl['dpd_range'] = self._acctSegCl['accountstatus'].apply(lambda x: x.split(' ')[0] if x.lower().find('days past due') > 0 else np.nan)
        # self._acctSegCl['dpd_range'] = self._acctSegCl['accountstatus'].apply(lambda x: re.search(r'\d+-\d+', x)[0] if re.search(r'\d+-\d+', x) is not None else np.nan)
        return
