import pandas as pd
import numpy as np
def count_nonzero_nonnan(data):
    count = 0
    for i in data:
        if not pd.isnull(i) and i != 0:
            count += 1
    return count


def pivot_cibil(data, col, val, suffix,nan_zero_flag=False):
    data1 = data.copy()
    data1 = data1.pivot(index='appid', columns=col, values=val)
    # data1.drop(['nan'], inplace = True, axis = 1)
    if nan_zero_flag:
        data1.replace(np.nan, 0, inplace=True)
    data1.columns = [str(column) + suffix for column in data1.columns]
    data1 = data1.reset_index()
    return data1