# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import warnings
import datetime
import logging
import os
import pandas as pd
import numpy as np
import traceback
import sys

from configs.config import LOGGER,bulk, INPUT
from configs.constant import final_var_list
from tests.test_id import test_dict
from feature_creation.accountSegment import total_aggregation,category_aggregation
from feature_creation import enquirySegment
from feature_creation import acct_enq_feature
from data_process.data_extract import DATA_EXTRACT
from data_process.data_cleaning import DATA_CLEAN

accountype_map = pd.read_csv(INPUT+"accountType_map.csv")

warnings.filterwarnings('ignore')
app_start_date = datetime.datetime.now()
print(app_start_date)
logging.basicConfig(filename=os.path.join(LOGGER, 'log_' + str(app_start_date.strftime('%d-%m-%Y_%H%M%S')) + '.log'), level=logging.INFO, format="%(asctime)s :: %(message)s", datefmt="%d-%m-%Y_%H:%M:%S")


def main_app():

    print("process Started...")
    mydf = pd.DataFrame()
    for scrub_dt, ID in test_dict.items():

        for i in range(0, len(ID), bulk):
            print("Loop:", i + 1)
            appids = list(ID[i: i + bulk])
            logging.info("scrub_date:: %s", str(scrub_dt))
            logging.info("appids: %s", ",".join(appids))

            try:
                de = DATA_EXTRACT()
                de.get_all_segments(appids, scrub_dt)
                print("loop Complated")
                dc = DATA_CLEAN(de)

                tagg = total_aggregation.TOTAL_AGGREGATE(dc)
                cagg = category_aggregation.CATEGORY_AGGREGATION(dc, accountype_map)
                eagg = enquirySegment.ENQUIRY_AGGREGATION(dc, accountype_map)

                final_df = pd.merge(tagg.total_cibil_loans, cagg.cibil_loans_agg_data, on='appid', how='left')
                final_df = pd.merge(final_df, eagg.final_enq, on='appid', how='left')
                final_df = acct_enq_feature.acct_to_enq(final_df)

                mydf = mydf.append(final_df)
            except Exception as ex:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                error = traceback.extract_tb(exc_tb)
                print(error)
                pass
    for f_var in final_var_list:
        if f_var not in mydf.columns:
            print(f_var)
            mydf[f_var] = np.nan
    return mydf
    # Press Ctrl+F8 to toggle the breakpoint.


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    output = main_app()
    print(datetime.datetime.now())
    print('Process Completed...')

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
