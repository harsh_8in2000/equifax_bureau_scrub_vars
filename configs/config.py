import os

DIR_PATH = os.getcwd()
INPUT = DIR_PATH + '//files//input//'
OUTPUT = DIR_PATH + '//files//output//'
LOGGER = DIR_PATH + '//logs//'

mode = 'file'
bulk = 10
data_mode = 'scrub'
