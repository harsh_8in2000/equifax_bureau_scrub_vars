acct_col_rename_dict = {'REFERENCE_NO':'appid','ACCT_NUMBER':'acct_number','ACCOUNTTYPE':'accounttype','ASSETCLASSIFICATION':'assetclassification','BALANCE':'balance','SANCTIONAMOUNT':'sanctionamount','COLLATERALTYPE':'collateraltype','COLLATERALVALUE':'collateralvalue','CREDITLIMIT':'creditlimit','DATEOPENED':'dateopened','DATEREPORTED':'datereported','HIGHCREDIT':'highcredit','INSTALLMENTAMOUNT':'installmentamount','LASTPAYMENT':'lastpayment','LASTPAYMENTDATE':'lastpaymentdate','OWNERSHIPTYPE':'ownershiptype','PASTDUEAMOUNT':'pastdueamount','REPAYMENTTENURE':'repaymenttenure','TERMFREQUENCY':'termfrequency','TERMS_FREQUENCY':'terms_frequency','SUITFILEDSTATUS':'suitfiledstatus','ACCOUNTSTATUS_HISTORY':'accountstatus_history','ASSETCLASS_HISTORY':'assetclass_history','SUITFILED_HISTORY':'suitfiled_history','ACCT_UNIQ_ID':'acct_uniq_id','CSMR_NBR':'csmr_nbr','SCORE':'bureau_score','SELF_TRADE':'self_trade','RETRO_DATE':'created_time','accountstatus':'accountstatus','DATECLOSED':'dateclosed','sector':'sector'}
enq_col_rename_dict = {'REFERENCE_NO':'appid','INQ_DATE':'enq_date','ProductType':'product_type','INQ_Amt':'enq_amt','Retro date':'created_time','Contributor':'contributor'}

var_list = [('M0' + str(i)) if i < 10 else ('M' + str(i)) for i in range(48)]
phd_var_list = [('M0' + str(i) + "_phd") if i < 10 else ('M' + str(i) + "_phd") for i in range(36)]
date_list_for_emi = [('D0' + str(i)) if i < 10 else ('D' + str(i)) for i in range(24)]
delinquencies = [7, 30, 60, 90]

dpd_var_list = ['1-29', '30-59', '60-89', '90-119', '120-179', '180-359', '360-539', '540-719', '720+']
final_var_list = ['appid', 'enq_cnt_6m', 'oldest_loan', 'consumer_enq_cnt_6m', 'consumer_enq_cnt_24m', 'enq_cnt_12m', 'Consumer Loan_enq_cnt_12m', 'credit_usage_ratio', 'currentBalance', 'account_enq_12m', 'cc_account_enq_12m', 'Consumer Loan_account_enq_6m', 'Consumer Loan_account_enq_12m', 'business_secure_current_os_bal', 'youngest_loan', 'non-collateralized_<=5L_youngest_loan', 'Two-wheeler Loan_oldest_loan', 'non-collateralized_<=5L_current_os_bal', 'min_disb_amt_18m', 'score', '1-29', '120-179', '180-359', '30-59', '360-539', '540-719', '60-89', '720+', '90-119', 'written_off_count', 'restructure_count', '1-29_ratio', '30-59_ratio', '60-89_ratio', '90-119_ratio', '120-179_ratio', '180-359_ratio', '360-539_ratio', '540-719_ratio', '720+_ratio', 'written_off_ratio', 'restructure_ratio', 'imputed_annual_income', 'bureau_thick_bad']

writtenof_status_map = {0: 'Restructured Loan',
1:'Restructured Loan (Govt. Mandated)',
2:'Written-off',
3:'Settled',
4:'Post (WO) Settled',
5:'Account Sold',
6:'Written Off and Account Sold',
7:'Account Purchased',
8:'Account Purchased and Written Off',
9:'Account Purchased and Settled',
10:'Account Purchased and Restructured',
11:'Restructured due to Natural Calamity',
12:'Restructured due to COVID-19',
99:'Clear existing status'
}

import re
writtenof_status = [x for x in list(writtenof_status_map.values()) if not re.search(r'Restructured|Clear',x)]
